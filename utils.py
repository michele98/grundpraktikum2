import numpy as np
import copy
import uncertainties
import string
import scipy.odr.odrpack as odrpack

class ShorthandFormatter(string.Formatter):

    def format_field(self, value, format_spec):
        if isinstance(value, uncertainties.UFloat):
            return value.format(format_spec+'S')  # Shorthand option added
        # Special formatting for other types can be added here (floats, etc.)
        else:
            # Usual formatting:
            return super(ShorthandFormatter, self).format_field(
                value, format_spec)

def fit_odr(f,x,y, xs, ys, beta_0):
    #ODR fitting
    #print ('\n')
    model = odrpack.Model(f)
    data = odrpack.RealData(x, y, sx=xs, sy=ys)
    myodr = odrpack.ODR(data, model, beta0=beta_0)
    output = myodr.run()
    #output.pprint()
    return output

def relu(arr):
    ret = np.zeros(len(arr))
    for i in range(len(arr)):
        if arr[i] > 0:
            ret[i] = arr[i]
    return ret

def weighted_average(values, errors):
    weights = np.array([1/error**2 for error in errors])
    vals = np.array(values)
    return sum(vals*weights)/sum(weights), np.sqrt(1/sum(weights))

def csv_to_list(filepath, delim = ","):
    with open(filepath, "r") as f:
        reader = csv.reader(f, delimiter = delim)
        my_list = list(reader)
    return my_list

def return_csv_row(table, index = 0, title = None, title_index = 0):
#see return_column for info
    row = []
    if title != None:
        for i in range(len(table)):
            if table[i][title_index] == title:
                index = i
    else:
        try:
            row.append(float(table[index][title_index]))
        except:
            row.append(table[index][title_index])
    
    for i in range(1+title_index, len(table[title_index])):
        try:
            row.append(table[index][i])
        except:
            print ("found non float element in csv")
    return row

def return_csv_column(table, index = 0, title = None, title_index = 0):
    #title_index is the row at which the title is
    column = []
    
    #if a title is given, it will set the column index as the one with that title
    if title != None:
        for i in range(len(table[title_index])):
            if table[title_index][i] == title:
                index = i

    #if no title is given, it returns the first column
    else:
        try: #tries to convert to float
            column.append(float(table[title_index][index]))
        except: #if it is not possible it just appends the item
            column.append(table[title_index][index])
    
    #starts from element 1 ignoring element 0 (which is usually a title)
    for i in range(1+title_index, len(table)):
        try:
            column.append(float(table[i][index]))
        except:
            print ("found non float element in csv")
    return column
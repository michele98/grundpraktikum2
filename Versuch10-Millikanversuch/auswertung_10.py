#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, weighted_average, red_chisquare

fmtr = ShorthandFormatter()

def linear_b(B,x):
    return B[0]*(1+8*10**(-5)*x)

def stdev(l):
    l = np.array(l)
    mean = sum(l)/len(l)
    return np.sqrt(sum((mean-l)**2)/(len(l)-1))

def linear(B,x):
    return B[0]*(1+B[1]*x)

def linear_2(x,a,b):
    return a*(1+b*x)

def get_data():
    xls = pd.ExcelFile('./daten_10.xlsx')
    df = pd.read_excel(xls, sheet_name = 'messwerte')
    df_c = pd.read_excel(xls, sheet_name = 'constants', index_col = 0)

    r = unp.uarray((df['r in mm']), df['r_err'])/1000
    U = unp.uarray(df['Spannung in V'], df['U_err'])
    V = unp.uarray((df['vol in mm^3']), df['vol_err'])/(1000**3)
    v2 = unp.uarray((df['v_rise']), df['v_rise_err'])

    d = np.array(df_c['value']['d in mm'])/1000
    eta = np.array(df_c['value']['eta in N s/m^2'])
    rho_air = np.array(df_c['value']['rho_l in kg/m^3'])
    rho_oil = np.array(df_c['value']['rho_o in kg/m^3'])
    p = unp.uarray(df_c['value']['p in Pa'], df_c['err']['p in Pa'])/100
    g = np.array(df_c['value']['g in m/s^2'])

    q = ((rho_oil-rho_air)*V*g+6*np.pi*r*eta*v2)*d/U
    group = np.array(df['gruppe'])

    return q*(10**19), r, p, group

def get_charge_groups(group):
    q, r, p, g = get_data()
    q1, x1 = zip(*[(elem[1], elem[2]) for elem in zip(g, q, 1/(r*unp.nominal_values(p))) if elem[0] == group])
    return np.array(q1), np.array(x1)

def write_txt_uncorrected_charge():
    q, x = zip(*[get_charge_groups(group) for group in [1,2]])
    txt = open('./ladungsgruppen.txt', 'w')

    q_avg = [weighted_average(unp.nominal_values(qi), unp.std_devs(qi)) for qi in q]
    q_avg = unp.uarray(list(zip(*q_avg))[0], list(zip(*q_avg))[1])

    q_avg_unw = [sum(unp.nominal_values(qi))/len(qi) for qi in q]
    q_stdev = [stdev(unp.nominal_values(qi)) for qi in q]

    for i in range(len(q)):
        q_23 = q[i]**(2/3)

        popt, cov = curve_fit(linear_2, unp.nominal_values(x[i]), unp.nominal_values(q_23), sigma = unp.std_devs(q_23), absolute_sigma = True)

        txt.write('-'*40+'\n\n')
        txt.write('Charge group {} (uncorrected):\n'.format(i+1))
        txt.write('Fit correlation factor = {:.3f}\n'.format(cov[0][1]/np.sqrt(cov[0][0]*cov[1][1])))
        txt.write('Everything in 10^-19 C\n\n')
        txt.write(fmtr.format('weighted average charge = {:.1u}\n', q_avg[i]))
        txt.write(fmtr.format('unweighted average charge = {:.3f}\n', q_avg_unw[i]))
        txt.write(fmtr.format('average charge std dev= {:.3f}\n', q_stdev[i]))
        txt.write(fmtr.format('average charge err= {:.3f}\n\n', q_stdev[i]/(np.sqrt(len(q[i])))))
        txt.write('-'*40 + '\n')

    txt.write(fmtr.format('\n\tDifference = {:.1u}', q_avg[1]-q_avg[0]))
    txt.close()

def plot_b_fixed(group):
    q, x = get_charge_groups(group)
    q_23 = q**(2/3)
    
    x_fit = np.linspace(1000, 3000, 10)
    output = fit_odr(linear_b, unp.nominal_values(x), unp.nominal_values(q_23), unp.std_devs(x), unp.std_devs(q_23), [0,0])
    chi2 = red_chisquare(unp.nominal_values(q_23), linear_b(output.beta, unp.nominal_values(x)), unp.std_devs(q_23), len(x)-len(output.beta))

    fig, ax = plt.subplots()

    ax.errorbar(unp.nominal_values(x), unp.nominal_values(q_23), xerr = unp.std_devs(x), yerr = unp.std_devs(q_23), fmt = 'bo', ecolor = 'k', capsize = 2, linewidth = 0.5, label = 'Messwerte')
    ax.plot(x_fit, linear_b(output.beta, x_fit), 'r-', linewidth = 1, label = r'linearer Fit $y=A(1+bx)$, $b$ fix')
    ax.plot([],[], ' ', label = fmtr.format(r'$A = {:.1u} \cdot (10^{{19}}\,$C)$^{{2/3}}$', ufloat(output.beta[0], output.sd_beta[0])))
    ax.plot([],[], ' ', label = fmtr.format(r'$q = {:.1u} \cdot 10^{{19}}\,$C', ufloat(output.beta[0], output.sd_beta[0])**(3/2)))
    ax.plot([],[], ' ', label = r'$\chi^2_\nu$ = {:.2f}'.format(chi2))

    ax.tick_params(labelsize = 18)
    ax.set_xlabel(r'$1/(r \cdot p)$ in (mbar m)$^{-1}$', fontsize = 18)
    ax.set_ylabel(r'$q\,^{\frac{2}{3}}$ in ($10^{{19}}\,$C)$^{\frac{2}{3}}$', fontsize = 18)

    if group == 1:
        ax.set_xbound(1320, 2745)
        #ax.set_ybound(1.44, 1.76)
        ax.set_ybound(1.44, 1.88)
        ax.legend(loc = 'upper right', fontsize = 'large')
        plt.subplots_adjust(left = 0.15, bottom = 0.14, right = 0.99, top = 0.99)

    elif group == 2:
        ax.set_xbound(1240, 1990)
        ax.set_ybound(2.21, 2.78)
        ax.legend(loc = 'lower right', fontsize = 'large')
        plt.subplots_adjust(left = 0.15, bottom = 0.14, right = 0.99, top = 0.99)

def plot_b(group):
    q, x = get_charge_groups(group)
    q_23 = q**(2/3)
    
    x_fit = np.linspace(1000, 3000, 10)
    output = fit_odr(linear, unp.nominal_values(x), unp.nominal_values(q_23), unp.std_devs(x), unp.std_devs(q_23), [0,0])
    chi2 = red_chisquare(unp.nominal_values(q_23), linear(output.beta, unp.nominal_values(x)), unp.std_devs(q_23), len(x)-len(output.beta))

    fig, ax = plt.subplots()

    ax.errorbar(unp.nominal_values(x), unp.nominal_values(q_23), xerr = unp.std_devs(x), yerr = unp.std_devs(q_23), fmt = 'bo', ecolor = 'k', capsize = 2, linewidth = 0.5, label = 'Messwerte')
    ax.plot(x_fit, linear(output.beta, x_fit), 'r-', linewidth = 1, label = r'linearer Fit $y=A(1+bx)$')
    ax.plot([],[], ' ', label = fmtr.format(r'$A = {:.1u}$, $b={:.1u}$', ufloat(output.beta[0], output.sd_beta[0]),ufloat(output.beta[1], output.sd_beta[1])))
    ax.plot([],[], ' ', label = fmtr.format(r'$q = {:.1u} \cdot 10^{{19}}\,$C', ufloat(output.beta[0], output.sd_beta[0])**(3/2)))
    ax.plot([],[], ' ', label = r'$\chi^2_\nu$ = {:.2f}'.format(chi2))

    ax.tick_params(labelsize = 18)
    ax.set_xlabel(r'$1/(r \cdot p)$', fontsize = 18)
    ax.set_ylabel(r'$q\,^{\frac{2}{3}}$ in $10^{{19}}\,$C$^{\frac{2}{3}}$', fontsize = 18)

    if group == 1:
        ax.set_xbound(1320, 2745)
        #ax.set_ybound(1.44, 1.76)
        ax.set_ybound(1.44, 1.88)
        ax.legend(loc = 'upper right', fontsize = 'large')
        plt.subplots_adjust(left = 0.15, bottom = 0.14, right = 0.99, top = 0.99)

    elif group == 2:
        ax.set_xbound(1240, 1990)
        ax.set_ybound(2.21, 2.78)
        ax.legend(loc = 'lower right', fontsize = 'large')
        plt.subplots_adjust(left = 0.15, bottom = 0.14, right = 0.99, top = 0.99)

def plot_hist():
    q, r, p, g = get_data()

    fig, ax = plt.subplots()
    bins = np.arange(0,4.6,0.05)
    #ax.hist(unp.nominal_values(q), bins, rwidth = 0.8 align = 'mid', color = '#FF9910', label = 'Häufigkeit pro 0,05 C;\nunkorrigiert') 
    ax.hist(unp.nominal_values(q), bins, align = 'mid', color = '#FF9910', label = r'Bins mit Länge $5\cdot 10^{-21}\,$C', edgecolor = 'k', linewidth = 0.8) #oder color FFFF00
    #ax.hist(unp.nominal_values(q), bins, align = 'mid', color = '#FF9910', label = r'Bins mit Länge $10^{-20}\,$C', edgecolor = 'k', linewidth = 0.8) #oder color FFFF00

    ax.tick_params(labelsize = 18)
    ax.set_xlabel(r'$q$ in $10^{-19}\,$C (unkorrigiert)', fontsize = 18)
    ax.set_ylabel('N', fontsize = 18)

    ax.set_xbound(1.5,4.3)

    ax.legend(loc = 'upper right', fontsize = 'x-large')

    plt.subplots_adjust(left = 0.11, bottom = 0.14, right = 0.99, top = 0.99)

if __name__=='__main__':
    plot_b_fixed(1)
    plot_b_fixed(2)
    plot_b(1)
    plot_b(2)
    plot_hist()
    
    write_txt_uncorrected_charge()
    
    plt.show()
#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu

def quartic(B, x):
    return B[0]*(x**4-B[1]**4)

def linear(B, x):
    return B[0]*x+B[1]

def quadratic(B,x):
    return B[0]*x*x+B[1]*x+B[2]

def fit_exponent(B,x):
    return B[0]*(x**B[2]-B[1]**B[2])

def linear(B,x):
    return B[0]*x+B[1]

def constant(B,x):
    return B[0]

def get_dataframe(sheet='Zener'):
    xls = pd.ExcelFile("./daten.xlsx")
    df = pd.read_excel(xls, sheet_name = sheet)
    return df

def get_data_zener():
    df = get_dataframe('Zener')
    u0 = df['u0 in V']
    u0_err = df['u0_err']
    ud = df['du in V']
    ud_err = df['ud_err']
    i = df['i in mA']
    i_err = df['i_err']
    return u0, u0_err, ud, ud_err, i, i_err

def calculate_zero(b=1,c=1):
    #b = ufloat(3.7, 0.1)*100
    #c = ufloat(-1.87, 0.07)*1000
    return -c/b

def plot_zener():
    u0, u0_err, ud, ud_err, i, i_err = get_data_zener()
    to_fit = 14
    output = fit_odr(linear, ud[-to_fit:], i[-to_fit:], ud_err[-to_fit:], i_err[-to_fit:], [1,1])
    b = ufloat(output.beta[0], output.sd_beta[0])
    c = ufloat(output.beta[1], output.sd_beta[1])

    x_fit = np.linspace(ud[len(ud)-to_fit]-0.5, max(ud), 400)
    y_fit = linear(output.beta, x_fit)

    fmtr = ShorthandFormatter()
    zero_str = fmtr.format('{0:.1u}', calculate_zero(b,c))
    b_str = fmtr.format('{0:.1u}', b)
    c_str = fmtr.format('{0:.1u}', c)
    fig, ax = plt.subplots()
    ax.errorbar(ud[-to_fit:], i[-to_fit:], xerr = ud_err[-to_fit:], yerr = i_err[-to_fit:], fmt= 'bs', capsize = 2, label = 'gefittete Datenpunkte', markersize = 5)
    ax.errorbar(ud[:-to_fit], i[:-to_fit], xerr = ud_err[:-to_fit], yerr = i_err[:-to_fit], fmt= '^', color = '#ff9c1b', capsize = 2, label = 'ausgelassene Datenpunkte im Fit')
    ax.plot(x_fit, y_fit, 'r-', label = r'linearer Fit mit $y=Bx+C$')
    #ax.plot([],[], ' ', label = r'mit B=3,7(1)e2 mA/V und C=-1,87(7)e3 mA')
    ax.plot([],[], ' ', label = r'mit B={} mA/V und C={} mA'.format(b_str, c_str))
    ax.plot([],[], ' ', label = r'$U_z = {}V$'.format(zero_str))
    ax.plot([min(ud), max(ud)+2], [0,0], 'k-', linewidth = 1)
    ax.errorbar([calculate_zero(b,c).nominal_value],[0], xerr = [calculate_zero(b,c).std_dev], fmt = 'gs', capsize = 3, zorder = 10)
    #labels
    ax.tick_params(labelsize = 18)
    ax.set_xlabel(r'$U_D$ in V', fontsize = 18)
    ax.set_ylabel('I in mA', fontsize = 18)
    ax.set_xbound(4.95, 5.55)
    ax.set_ybound(-10, 180)

    #legend
    ax.legend(fontsize = 'x-large', loc = 'upper left')

    #fig.set_size_inches((8,6))
    plt.tight_layout()
    plt.subplots_adjust(left = 0.15, right = 0.99, bottom = 0.14, top = 0.99)

def get_data_jfet():
    df = get_dataframe('Transistor')
    df = df.sort_values('ugs in mV',axis = 0, ascending = True)
    ugs = df['ugs in mV']/1000
    ugs_err = df['ugs_err']/1000
    i = df['i']
    i_err = df['i_err']
    return np.array(ugs), np.array(ugs_err), np.array(i), np.array(i_err)

def calculate_saturation(a,b,c,i_s):
    u0 = -b/(2*a)
    c2 = c+a*u0*u0+b*u0
    return sqrt((i_s-c2)/a)+u0

def calculate_current_min(a,b,c):
    return (4*a*c-b*b)/(4*a)

def plot_jfet():
    ugs, ugs_err, i, i_err = get_data_jfet()
    to_fit = 18
    #output = fit_odr(linear, ugs[:to_fit], i[:to_fit], ugs_err[:to_fit], i_err[:to_fit], [1,1])
    output = fit_odr(quadratic, ugs[:to_fit], i[:to_fit], ugs_err[:to_fit], i_err[:to_fit], [1,4,1])
    x_fit = np.linspace(min(ugs)-1, ugs[to_fit+5], 400)
    y_fit = quadratic(output.beta, x_fit)
    a = ufloat(output.beta[0], output.sd_beta[0])
    b = ufloat(output.beta[1], output.sd_beta[1])
    c = ufloat(output.beta[2], output.sd_beta[2])
    i_s = ufloat(max(i), i_err[-1])
    u_s = calculate_saturation(a,b,c,i_s)
    c2, p = chisquare(i, quadratic(output.beta, ugs))
    
    #write fit parameters to strungs
    fmtr = ShorthandFormatter()
    a_str = fmtr.format('{0:.1u}', a)
    b_str = fmtr.format('{0:.1u}', b)
    c_str = fmtr.format('{0:.1u}', c)
    is_str = fmtr.format('{0:.1u}', i_s)
    us_str = fmtr.format('{0:.1u}', u_s)

    fig, ax = plt.subplots()
    ax.plot(x_fit, y_fit, 'b-', label = r'Parabelfit mit $y=Ax^2+Bx+C$')
    ax.plot([],[], ' ', label = r'$A={}\,mA/V^2$, $B={}\,mA/V$'.format(a_str, b_str))
    ax.plot([],[], ' ', label = r'und $C={}\,mA$;  $\chi^2={:.2f}$'.format(c_str, c2))
    ax.plot([min(ugs)-2, max(ugs)+2], [i[-1],max(i)], 'k-', linewidth = 1, label = u'Sättigungsstrom: ' + r'$I_s={}\,mA$'.format(is_str))
    #ax.plot([],[], ' ', label = r'$I_s={}mA$'.format(is_str))
    ax.plot([u_s.nominal_value, u_s.nominal_value],[0,20], 'k--',linewidth = 1, label = u'Sättigungsspannung: '+'$U_s = {}\,V$'.format(us_str))
    ax.errorbar([u_s.nominal_value],[i_s.nominal_value], xerr = [u_s.std_dev], yerr = [i_err[to_fit]], fmt = 'ko', capsize = 3, zorder = 10, markersize = 7)
    ax.errorbar(ugs[:to_fit], i[:to_fit], xerr = ugs_err[:to_fit], yerr = i_err[:to_fit], fmt= 'bs', capsize = 2, label = 'gefittete Datenpunkte', markersize = 6)
    ax.errorbar(ugs[to_fit:], i[to_fit:], xerr = ugs_err[to_fit:], yerr = i_err[to_fit:], fmt= 'r^', capsize = 2, label = 'ausgelassene Datenpunkte im Fit', markersize = 6)#, color = '#ff9c1b')

    #labels
    ax.tick_params(labelsize = 18)
    ax.set_xlabel(r'$U_{GS}$ in V', fontsize = 18)
    ax.set_ylabel('I in mA', fontsize = 18)
    ax.set_xbound(-3.1, 2.2)
    ax.set_ybound(0, 21)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    #legend
    #ax.legend(fontsize = 'large', loc = 'upper left')

    # legend with removed errorbar handles
    handles, labels = ax.get_legend_handles_labels()
    handles = [h[0] if isinstance(h, container.ErrorbarContainer) else h for h in handles]
    #legend = ax.legend(handles, labels, bbox_to_anchor=(1, 0.3), loc='center right', borderaxespad=0., fontsize = 'x-large')
    legend = ax.legend(handles, labels, bbox_to_anchor=(1.28, 0.33), loc='center right', borderaxespad=0., fontsize = 'xx-large')
    #legend.get_title().set_fontsize('14')


    fig.set_size_inches((10,6))
    #plt.tight_layout()
    plt.subplots_adjust(left = 0.1, right = 0.8, bottom = 0.115, top = 1)

if __name__ == "__main__":
    plot_zener()
    plot_jfet()
    plt.show()
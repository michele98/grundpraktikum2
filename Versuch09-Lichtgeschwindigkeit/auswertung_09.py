#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
import cv2

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu, return_csv_column, csv_to_list

fmtr = ShorthandFormatter()

def gaussian(B,x):
    return B[0]*np.exp(-((x-B[2])/B[1])**2/2)+B[3]

def gaussian_2(x,A,s,xo,offset):
    return A*np.exp(-(x-xo)**2/s)+offset

def linear(B,x):
    return B[0]*x+B[1]

def get_data_calibration():
    df = pd.read_csv('./data/data.csv')
    d = np.array(df['d'][:5])
    d_err = np.array(df['d_err'][:5])
    x_max = np.array(df['X_0'][:5])
    x_max_err = np.array(df['X_0_err'][:5])
    return d, d_err, x_max, x_max_err

def plot_calibration():
    d, d_err, x_max, x_max_err = get_data_calibration()

    output = fit_odr(linear,d,x_max,d_err,x_max_err,[-1,1])
    x_fit = np.linspace(min(d), max(d), 100)

    fig, ax = plt.subplots()
    ax.errorbar(d,x_max, xerr = d_err, yerr = x_max_err, fmt = 'b.', markersize = 4, capsize = 2, label = 'Messpunkte')
    ax.plot(x_fit, linear(output.beta,x_fit), 'r-', label = 'linearer Fit y=ax+b')
    ax.plot([],[],' ', label = fmtr.format(r'a={:.1u}mm$^{{-1}}$, b={:.1u}', ufloat(output.beta[0], output.sd_beta[0]), ufloat(output.beta[1], output.sd_beta[1])))

    ax.tick_params(labelsize = 18)

    #ax.set_xbound(600,1250)
    #ax.set_ybound(0,1450)

    ax.set_xlabel('Einstellung der Mikrometerschraube in mm', fontsize = 18)
    ax.set_ylabel(u'Position des Maximums in Pixel', fontsize = 18)
    ax.legend(loc = 'upper right', fontsize = 14)
    plt.subplots_adjust(left = 0.15, bottom = 0.14, right = 0.98, top = 0.99)

def stdev(n):
    mean = sum(n)/len(n)
    return np.sqrt(sum((mean-np.array(n))**2/(len(n)-1)))

def calculate_err(f,x_max):
    f*=6
    indexes = [(frequency,[i for i, j in enumerate(f) if j == frequency]) for frequency in range(1000,5001,1000)]
    err = []
    for info in indexes:
        frequency = info[0]
        err.append(stdev(x_max[info[1][0]:info[1][-1]]))
    return (sum(err[:-1])/(len(err)-1))

def merge_arrays(f,x):
    same = []
    z = sorted(list(zip(f,x)))
    f,x = zip(*z)
    for i in range(1,len(f)-1):
        if f[i]!=f[i+1] or i==len(f)-2:
            new_indexes=[]
            j = 0
            while f[i]==f[i-j]:
                new_indexes.append(i-j)
                j+=1
            same.append(list(reversed(new_indexes)))
    same[-1].append(34)

    new_f = [f[indexes[0]] for indexes in same]
    new_x = [sum(x[indexes[0]:indexes[-1]+1])/len(indexes) for indexes in same]
    new_x_err = [stdev(x[indexes[0]:indexes[-1]+1]) for indexes in same]
    import math
    for i in range(len(new_x_err)):
        if math.isnan(new_x_err[i]): new_x_err[i] = (new_x_err[i-1]+new_x_err[i+1])/2
    return np.array(new_f), np.array(new_x), np.array(new_x_err)

def get_data_frequency():
    df = pd.read_csv('./data/data_python_background_removed_filtered.csv')
    #df = pd.read_csv('./data/data_filter.csv')
    f = np.array(df['f'][7:])/6
    x_max = np.array(df['X_0'][7:])-136
    x_max_err = np.array(df['X_0_err'][7:])
    #x_max_err = calculate_err(f,x_max)
    f,x_max,x_max_err =  merge_arrays(f,x_max)
    x_max_err = np.zeros(len(f))+0.5
    #x_max_err[-2] = x_max_err[-1]
    return f, x_max, x_max_err

def calculate_c(slope, a=1000):
    slope = 1000*slope/a
    omega_s = 2*np.pi/slope #in pixel/um
    d1 = ufloat(1224,1) + ufloat(1360,1) + ufloat(1280,1) + ufloat(8,0.005) #BC + CD + BH + Kamera
    d2 = ufloat(860,1) - ufloat(30.7,0.2)/2 #DE - Radius Spiegel
    d3 = ufloat(1441,1) - ufloat(30.7,0.2)/2 + ufloat(1844,1) # EF - Radius Spiegel + FG
    print (d1, d2, d3)
    return 4*d1*d3*d3*omega_s/(d2+d3)

def calculate_c_stat(slope, a=1000):
    slope = 1000*slope/a
    omega_s = 2*np.pi/slope #in pixel/um
    d1 = 1224+1360+1280+8 #BC + CD + BH + Kamera
    d2 = 860-30.7/2 #DE - Radius Spiegel
    d3 = 1441-30.7/2+1844 # EF - Radius Spiegel + FG
    print (d1, d2, d3)
    return 4*d1*d3*d3*omega_s/(d2+d3)

def plot_frequency():
    f, x_max, x_max_err = get_data_frequency()
    bis = -1
    f,x_max,x_max_err = f[:bis], x_max[:bis], x_max_err[:bis]

    conversion = 103.6
    conversion_err = 0.1

    d = 1000*x_max/conversion
    d_err = 1000*x_max_err/conversion

    output = fit_odr(linear,f,d,None,d_err,[-1,1])
    output_pixel = fit_odr(linear,f,x_max,None,d_err,[-1,1])
    x_fit = np.linspace(min(f), max(f), 100)

    c_stat = calculate_c_stat(ufloat(output.beta[0],output.sd_beta[0])) #nur statistische Fehler
    c_sys = calculate_c(output_pixel.beta[0], conversion) #auch systematische Fehler
    c = calculate_c(ufloat(output_pixel.beta[0],output_pixel.sd_beta[0]), ufloat(conversion, conversion_err)) #auch systematische Fehler
    print (c_stat, c_sys, c)

    fig, ax = plt.subplots()
    ax.errorbar(f, d, yerr=d_err, fmt= 'b.', markersize = 4, capsize = 2, label = 'Messpunkte')
    ax.plot(x_fit, linear(output.beta,x_fit), 'r-', label = 'linearer Fit y=ax+b')
    ax.plot([],[],' ', label = fmtr.format(r'a={:.1u}$\,\mu$m/Hz, b={:.1u}$\,\mu$m', ufloat(output.beta[0], output.sd_beta[0]), ufloat(output.beta[1], output.sd_beta[1])))
    ax.plot([],[],' ', label = fmtr.format(r'c={:.2u} m/s', c))

    #ax.errorbar(f, x_max, yerr=x_max_err, fmt= 'b.', markersize = 4, capsize = 2, label = 'Messpunkte')
    #ax.plot(x_fit, linear(output_pixel.beta,x_fit), 'r-', label = 'linearer Fit y=ax+b')
    #ax.plot([],[],' ', label = fmtr.format(r'a={:.1u} Hz$^{{-1}}$, b={:.1u}', ufloat(output_pixel.beta[0], output_pixel.sd_beta[0]), ufloat(output_pixel.beta[1], output_pixel.sd_beta[1])))
    #ax.plot([],[],' ', label = fmtr.format(r'c={:.1u} m/s', c))

    ax.tick_params(labelsize = 18)

    ax.set_xlabel('Drehfrequenz in Hz', fontsize = 18)
    ax.set_ylabel(r'Position des Maximums in $\mu$m', fontsize = 18)
    ax.legend(loc = 'upper left', fontsize = 12)
    plt.subplots_adjust(left = 0.15, bottom = 0.14, right = 0.98, top = 0.99)

if __name__=='__main__':
    d = {'calibration':[(2.45,1),(3,2),(3.5,3),(4,4),(4.5,5)],\
        'frequency':[(500,(7,27)), (750,(8,)), (1000,(9,28,38)), (1500,(11,29)), (1750,(12,)),\
        (2000,(13,30,39)), (2250,(14,)), (2500,(15,31)), (2750,(16,)), (3000,(17,32,40)), (3250,(18,)), (3500,(19,33)),\
            (3750,(20,)), (4000,(21,34,41)), (4250,(22,)), (4500,(23,35)), (4750,(24,26)), (5000,(25,36,37,42))]}

    plot_calibration()
    plot_frequency()
    plt.show()

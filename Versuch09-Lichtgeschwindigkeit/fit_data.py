import numpy as np
import cv2
import matplotlib.pyplot as plt
import uncertainties.unumpy as unp
from uncertainties import ufloat

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu, return_csv_column, csv_to_list

fmtr = ShorthandFormatter()

def gaussian(B,x):
    return B[0]*np.exp(-((x-B[2])/B[1])**2/2)+B[3]

def read_rgb(path):
    im = cv2.imread(path)
    b,g,r = cv2.split(im)
    return cv2.merge([r,g,b])

def filter_line(im, line, line_height):
    if np.shape(im.shape)==(2,):
        r = np.zeros(len(im[line]))
        for i in range(line_height):
            r+=im[line-int(line_height/2)+i]/int(line_height)
        return r
    else:
        r = np.zeros((3,len(im[line])))
        for j in range(3):
            for i in range(line_height):
                r[j]+=im[line-int(line_height/2)+i,:,j]/int(line_height)
        return r

def smooth(data,filter_length):
    if filter_length==1: return data
    #smooths a curve by taking the averave value of each neighboring data point in filter_length range
    filter_length = int(filter_length/2)
    data_2 = np.zeros(len(data)+2*filter_length)
    for i in range(filter_length):
        data_2[i] = data[0]
        data_2[-i] = data[-1]
    data_2[filter_length:-filter_length]+=data
    data = data_2
    return np.array([np.average(data[i-filter_length:i+filter_length]) for i in range(filter_length,len(data)-filter_length)])

def draw_line(im, line, height, color =(255,255,255) ,alpha = 1):
    overlay = im.copy()
    output = im.copy()
    cv2.line(overlay,(0,line),(1280,line),color,height)
    return cv2.addWeighted(overlay, alpha, output, 1-alpha, 0, output)

def fit_gaussian(im, line=None, line_height=None, smooth_width=1, fit_range = None, return_data = False, p0 = [30,35,150,20]):
    if line == None: line = int(len(im)/2)
    if line_height == None: line_height = len(im)

    x = np.arange(1,len(im[0])+1,1)
    signal = smooth(filter_line(im,line,line_height),smooth_width)

    if fit_range == None: fit_range = (0,len(im[0]))

    output = fit_odr(gaussian,x[fit_range[0]:fit_range[1]],signal[fit_range[0]:fit_range[1]],None,None, p0)

    if return_data: return x, signal, output
    return output

def write_fit_file(ims, line=None, line_height = None, smooth_width=1, background = None, filename = 'boh.csv'):
    if type(background)!=type(None): ims = [cv2.convertScaleAbs(np.float32(ims[i])-np.float32(background)) for i in range(len(ims))]

    f = open('./data/' + filename,'w')
    f.write('Nummer,a,a_err,X_0,X_0_err,S,S_err,b,b_err\n')
    for i in range(len(ims)):
        output = fit_gaussian(ims[i], line, line_height, smooth_width)
        par, p_err = output.beta, output.sd_beta

        f.write('{},'.format(i+1))
        f.write('{},{},{},{},'.format(par[0],p_err[0],par[2],p_err[2]))
        f.write('{},{},{},{}\n'.format(par[1],p_err[1],par[3],p_err[3]))
    f.close()

def write_filter_file(ims, line=None, line_height = None, smooth_width=1, background=None, filename = 'boh.csv'):
    if line == None: line = int(len(im)/2)
    if line_height == None: line_height = len(im)

    if type(background)!=type(None): ims = [cv2.convertScaleAbs(np.float32(ims[i])-np.float32(background)) for i in range(len(ims))]

    f = open('./data/' + filename,'w')
    f.write('Nummer,X_0,X_0_err\n')
    for i in range(len(ims)):
        #signal = smooth(filter_line(cv2.convertScaleAbs(np.float32(ims[i])-np.float32(im_back)),line,line_height),smooth_width)
        signal = smooth(filter_line(ims[i],line,line_height),smooth_width)
        max_signal_index = np.argmax(signal)

        f.write('{},'.format(i+1))
        f.write('{},{}\n'.format(max_signal_index,0.5))
    f.close()

def intersect_images():
    ims = [cv2.imread("./data/original/{}.bmp".format(i+1),0) for i in range(6,42)]
    ims_32 = [np.float32(im) for im in ims]

    im_min = np.zeros(ims[0].shape)
    for i in range(len(im_min)):
        for j in range(len(im_min[0])):
            im_min[i][j] = min([im[i][j] for im in ims_32])
    return cv2.convertScaleAbs(im_min)

def save_corrected_images(ims, gauss_coefficient, enhance = 1):
    im_back = intersect_images()

    x, signal, output_x = fit_gaussian(np.float32(im_back), line = 160, line_height = 120, return_data=True)
    y, singal, output_y = fit_gaussian(np.transpose(np.float32(im_back)), line = 175, line_height = 70, return_data=True)

    new = np.zeros((len(y),len(x)))
    for i in range(len(y)):
        for j in range(len(x)):
            new[i][j] = gauss_coefficient*gaussian([*output_x.beta[:-1],0], x[j])*gaussian([*output_y.beta[:-1],0], y[i])
    im_back_corr_32 = np.float32(im_back)-new

    import os
    new_dir = './data/corrected_{:.0f}'.format((100*gauss_coefficient))
    try:
        os.mkdir(new_dir)
    except:
        print('directory alredy exists, overwritten everything')

    cv2.imwrite('./data/backgrounds/background_0{:.0f}.bmp'.format(100*gauss_coefficient), cv2.convertScaleAbs(im_back_corr_32))
    cv2.imwrite('./data/backgrounds/e_background_0{:.0f}.bmp'.format(100*gauss_coefficient), cv2.convertScaleAbs(enhance*im_back_corr_32))

    cv2.imwrite('./data/backgrounds/gauss_fit_0{:.0f}.bmp'.format(100*gauss_coefficient), cv2.convertScaleAbs(new))
    cv2.imwrite('./data/backgrounds/e_gauss_fit_0{:.0f}.bmp'.format(100*gauss_coefficient), cv2.convertScaleAbs(enhance*new))

    for i in range(6):
        cv2.imwrite('{}/{}.bmp'.format(new_dir, i+1), cv2.convertScaleAbs(enhance*(np.float32(ims[i]))))
    for i in range(6,len(ims)):
        cv2.imwrite('{}/{}.bmp'.format(new_dir, i+1), cv2.convertScaleAbs(enhance*(np.float32(ims[i])-im_back_corr_32)))

def plot_image_luminance(im, line=None, line_height=None, smooth_width=1, fit_range = None, background = None, green = False, fit = True, p0 = [30,35,150,20], enhance = 1, one_fig = True):
    if fit_range == None: fit_range = (0,len(im[0]))
    x_fit = np.linspace(fit_range[0], fit_range[1], 200)
    
    if type(background)!=type(None): im = cv2.convertScaleAbs(np.float32(im)-np.float32(background))

    x, signal, output = fit_gaussian(im, line, line_height, smooth_width, return_data=True, p0=p0)
    max_signal_index = np.argmax(signal)

    if one_fig:
        fig,axs = plt.subplots(ncols=2)
    else:
        fig0,ax0 = plt.subplots()
        fig1,ax1 = plt.subplots()
        axs = [ax0, ax1]
    #axs[0].plot(x,signal,'b.', label = 'line {}, width {}, signal filter {}'.format(line,line_height,smooth_width), markersize = 1)
    #axs[0].plot(x[max_signal_index],signal[max_signal_index], 'ro', label = 'Maximum {}'.format(max_signal_index))
    axs[0].plot(x,signal,'b.', label = 'Messpunkte', markersize = 2)

    if fit:
        #axs[0].plot(x_fit, gaussian(output.beta,x_fit), 'r-', label = fmtr.format('Fit odr, max {:.1u}', ufloat(output.beta[2], output.sd_beta[2])))
        axs[0].plot(x_fit, gaussian(output.beta,x_fit), 'r-', label = r'Gaussfit $y=A\,exp\left(\frac{(x-x_0)^2}{2\sigma^2}\right)+b$')
        axs[0].plot([],[],' ', label = fmtr.format(r'$A={:.1u}$, $x_0={:.1u}$', ufloat(output.beta[0], output.sd_beta[0]), ufloat(output.beta[2], output.sd_beta[2])))
        axs[0].plot([],[],' ', label = fmtr.format(r'$\sigma={:.1u}$, $b={:.1u}$', ufloat(output.beta[1], output.sd_beta[1]),  ufloat(output.beta[3], output.sd_beta[3])))

    im2 = cv2.merge([im,im,im])
    if green:
        imd = draw_line(im2,line,line_height, (120,255,0), 0.05)
        axs[1].imshow(enhance*imd, cmap = 'gray')
    else:
        axs[1].imshow(enhance*im2, cmap = 'gray')

    axs[0].legend(loc = 'center left', fontsize = 12)
    axs[0].set_xbound(0,len(im[0]))
    axs[0].set_ylabel(u'Mittelwert der Signalintensität', fontsize = 16)

    for ax in axs:
        ax.tick_params(labelsize = 18)
        ax.set_xlabel('Position in Pixel', fontsize = 18)

    if one_fig: fig.set_size_inches((12,5))
    plt.subplots_adjust(left = 0.065, bottom = 0.14, right = 1, top=0.98)

if __name__=='__main__':
    correction_value = '025'
    ims = [cv2.imread("./data/original/{}.bmp".format(i+1),0) for i in range(42)]
    ims_corr = [cv2.imread("./data/corrected_{}/{}.bmp".format(correction_value,i+1),0) for i in range(42)]

    im_name = '22.bmp'
    im = cv2.imread("./data/original/{}".format(im_name),0)
    im_corr = cv2.imread("./data/corrected_{}/{}".format(correction_value,im_name),0)

    im_back = cv2.imread("./data/backgrounds/background_025.bmp",0)

    line, line_width, smooth_width = int(len(im)/2),len(im), 1
    #line, line_width, smooth_width = 50, 50, 1

    #save_corrected_images(ims, 0.3, 3)
    #save_corrected_images(ims, 0.3, 1)
    #plot_image_luminance(ims[1],line, line_width, smooth_width, fit = False, p0=[33,12,278,34])
    #plot_image_luminance(ims[1],line, line_width, smooth_width, green = False, p0=[33,12,221,34], enhance = 2)
    #plot_image_luminance(im,line, line_width, smooth_width, enhance = 3, one_fig = False)
    #plot_image_luminance(im_corr,line, line_width, smooth_width, enhance = 3, one_fig = False)
    plot_image_luminance(im_back,line, line_width, smooth_width, enhance = 5, fit = False)
    plt.show()

    #write_fit_file(ims_corr, line, line_width, smooth_width, filename='boh.csv')

    #ims = [cv2.imread("./data/original/{}.bmp".format(i+1),0) for i in range(5)]
    #im2 = ims[0]+ims[-1]-37
    #im2 = cv2.convertScaleAbs((sum(np.float32(ims))-(len(ims)-1)*37))
    #cv2.imshow('boh',im2)
    #cv2.waitKey(0)

import pandas as pd

def write_data_frequency(d,df):
    f = open('./data/data_frequency.csv','w')
    f.write('f,a,a_err,X_0,X_0_err,S,S_err,b,b_err\n')
    for info in d['frequency']:
        for pic_number in info[1]:
            f.write('{},'.format(info[0]))
            f.write('{},{},{},{},'.format(df['a'][pic_number], df['a_err'][pic_number], df['X_0'][pic_number], df['X_0_err'][pic_number]))
            f.write('{},{},{},{}\n'.format(df['S'][pic_number], df['S_err'][pic_number], df['b'][pic_number], df['b_err'][pic_number]))
    f.close()

def write_data_calibration(d,df):
    f = open('./data/data_calibration.csv','w')
    f.write('n,d,d_err,a,a_err,X_0,X_0_err,S,S_err,b,b_err\n')
    d_err = 0.001
    for info in d['calibration']:
        f.write('{},{},{},'.format(info[1],info[0],d_err))
        f.write('{},{},{},{},'.format(df['a'][info[1]], df['a_err'][info[1]], df['X_0'][info[1]], df['X_0_err'][info[1]]))
        f.write('{},{},{},{}\n'.format(df['S'][info[1]], df['S_err'][info[1]], df['b'][info[1]], df['b_err'][info[1]]))
    f.close()

if __name__=='__main__':
    d = {'calibration':[(2.45,1),(3,2),(3.5,3),(4,4),(4.5,5)],\
        'frequency':[(500,(7,27)), (750,(8,)), (1000,(9,28,38)), (1500,(11,29)), (1750,(12,)),\
        (2000,(13,30,39)), (2250,(14,)), (2500,(15,31)), (2750,(16,)), (3000,(17,32,40)), (3250,(18,)), (3500,(19,33)),\
            (3750,(20,)), (4000,(21,34,41)), (4250,(22,)), (4500,(23,35)), (4750,(24,26)), (5000,(25,36,37,42))]}
    df = pd.read_csv('./data/data.csv', index_col=0)

    write_data_frequency(d,df)
    write_data_calibration(d,df)
#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare

import sys
sys.path.insert(0,"../../")
from utils import ShorthandFormatter, fit_odr, relu

def quartic(B, x):
    return B[0]*(x**4-B[1]**4)

def linear(B, x):
    return B[0]*x+B[1]

def fit_exponent(B,x):
    return B[0]*(x**B[2]-B[1]**B[2])

def linear(B,x):
    return B[0]*x+B[1]

def get_dataframe():
    xls = pd.ExcelFile("../ofen.xlsx")
    df = pd.read_excel(xls, sheet_name = 'kuehlung')
    return df

def get_data():
    df = get_dataframe()
    temp = df['abs_temp']
    temp_err = df['temp_err']
    voltage = df['voltage']
    voltage_err = df['voltage_err']
    return temp, temp_err, voltage, voltage_err

def dydx(x, y):
    #returns new x axis with average and derivative in y axis
    return np.array([(x[i]+x[i-1])/2 for i in range(1,len(x))]), np.array([(y[i]-y[i-1])/(x[i]-x[i-1]) for i in range(1,len(x))])

def get_fit_params():
    temp, temp_err, voltage, voltage_err = get_data()
    output = fit_odr(quartic, temp, voltage, temp_err, voltage_err, [1,400])
    return output.beta[0], output.sd_beta[0], output.beta[1], output.sd_beta[1]

def plot_linear():
    temp, temp_err, voltage, voltage_err = get_data()
    fig, ax = plt.subplots()

    #fitting
    output = fit_odr(quartic, temp, voltage, temp_err, voltage_err, [1,400])
    output_exp = fit_odr(fit_exponent, temp, voltage, temp_err, voltage_err, [1,400,4])
    print (output_exp.beta, output_exp.sd_beta)
    print(output.beta, output.sd_beta)
    x_fit = np.linspace(min(temp)-100, max(temp)+100, 800)
    #x_fit = np.linspace(min(temp), max(temp), 800)
    #y_fit = quartic(output.beta, x_fit)
    y_fit_old = quartic(output.beta, x_fit)
    y_fit = fit_exponent(output_exp.beta, x_fit)
    
    #formatting
    fmtr = ShorthandFormatter()
    sigma2_str = fmtr.format('{0:.2u}', ufloat(output_exp.beta[0], output_exp.sd_beta[0]))
    sigma2_str = r'1.14(67)\cdot10^{-11}'
    sigma2_old_str = fmtr.format('{0:.2u}', ufloat(output.beta[0], output.sd_beta[0]))
    #sigma2_old_str = r'2.420(14)\cdot 10^{-11}'
    t2_old_str = fmtr.format('{0:.1u}', ufloat(output.beta[1], output.sd_beta[1]))
    t2_str = fmtr.format('{0:.1u}', ufloat(output_exp.beta[1], output_exp.sd_beta[1]))
    n_str = fmtr.format('{0:.1u}', ufloat(output_exp.beta[2], output_exp.sd_beta[2]))
    
    #units strings
    sigma_unit_str = r'\frac{W}{m^2 \cdot K^4}'
    sigma2_unit_str = r'\frac{mV}{K^4}'
    
    #plot
    ax.errorbar(temp, voltage, xerr = temp_err, yerr = voltage_err, fmt = 'b.', label = 'Datenpunkte', capsize = 2)
    ax.plot(x_fit, y_fit, 'r-', label = r"gefittete Kurve y = $\sigma' (T^n-T'^n)$")
    ax.plot([],[],' ', label = "mit $n={}$, $\sigma' = {}\,{}$ und $T' = {}\,K$".format(n_str, sigma2_str, sigma2_unit_str, t2_str))
    #ax.plot(x_fit, y_fit_old, 'y-', label = r"gefittete Kurve y = $\sigma' (T^4-T'^4)$")
    #ax.plot([],[],' ', label = "mit $\sigma' = {}\,{}$ und $T' = {}\,K$".format(sigma2_old_str, sigma2_unit_str, t2_old_str))
    #c2, p = chisquare(voltage, quartic(output.beta, temp))
    c2, p = chisquare(voltage, fit_exponent(output_exp.beta, temp))
    ax.plot([],[],' ', label = r"$\chi^2_\nu={:.2f}$".format(c2))
    
    #plot appearence
    ax.tick_params(labelsize = 16)
    
    #labels
    ax.set_xlabel('Temperautur in K', fontsize = 18)
    ax.set_ylabel('Spannung am Detektor in mV', fontsize = 18)

    ax.set_xbound(370, 645)
    ax.set_ybound(0, 4)

    #legend
    ax.legend(fontsize = 'large')

    #fig.set_size_inches((8,6))
    plt.tight_layout()
    plt.subplots_adjust(left = 0.12, right = 0.99, bottom = 0.13, top = 0.98)

def plot_log():
    temp, temp_err, voltage, voltage_err = get_data()
    sigma2, sigma2_err, t2, t2_err = get_fit_params()
    x_fit = np.linspace(min(temp)-100, max(temp)+100, 800)
    #x_fit = np.linspace(min(temp), max(temp), 800)
    y_fit = sigma2*(x_fit**4-t2**4)
    
    fig, ax = plt.subplots()
    ax.errorbar(temp, voltage, xerr = temp_err, yerr = voltage_err, fmt = 'b.', label = 'Datenpunkte', capsize = 2)
    ax.plot(x_fit, y_fit, 'r-', label = "$y=\log(\sigma') + \log(T^4-T'^4)$")

    #logarithmic scale
    ax.set_yscale('log')
    ax.set_xscale('log')

    #plot appearence
    ax.tick_params(labelsize = 16)
    
    #labels
    ax.set_xlabel('Temperautur in K', fontsize = 18)
    ax.set_ylabel('Spannung am Detektor in mV', fontsize = 18)
    ax.set_xbound(378, 650)
    ax.set_ybound(0.24, 4)

    #legend
    ax.legend(fontsize = 'x-large')

    #fig.set_size_inches((8,6))
    plt.tight_layout()
    plt.subplots_adjust(left = 0.13, right = 0.99, bottom = 0.12, top = 0.99)

def plot_log_2():
    temp, temp_err, voltage, voltage_err = get_data()
    output = fit_odr(fit_exponent, temp, voltage, temp_err, voltage_err, [1,400,4])

    x_fit = np.linspace(min(temp)-100, max(temp)+100, 800)
    y_fit = fit_exponent(output.beta, x_fit)
    
    fmtr = ShorthandFormatter()
    sigma2_str = fmtr.format('{0:.2u}', ufloat(output.beta[0], output.sd_beta[0]))
    sigma2_str = r'1.14(67)\cdot10^{-11}'
    t2_str = fmtr.format('{0:.1u}', ufloat(output.beta[1], output.sd_beta[1]))
    n_str = fmtr.format('{0:.1u}', ufloat(output.beta[2], output.sd_beta[2]))
    
    #units strings
    sigma2_unit_str = r'\frac{mV}{K^4}'

    fig, ax = plt.subplots()
    ax.errorbar(temp, voltage, xerr = temp_err, yerr = voltage_err, fmt = 'b.', label = 'Datenpunkte', capsize = 2)
    ax.plot(x_fit, y_fit, 'r-', label = "$y=\log(\sigma') + \log(T^n-T'^n)$")
    ax.plot([],[],' ', label = "mit $n={}$, $\sigma' = {}\,{}$ und $T' = {}\,K$".format(n_str, sigma2_str, sigma2_unit_str, t2_str))
    #logarithmic scale
    ax.set_yscale('log')
    ax.set_xscale('log')

    #plot appearence
    ax.tick_params(labelsize = 16)
    
    #labels
    ax.set_xlabel('Temperautur in K', fontsize = 18)
    ax.set_ylabel('Spannung am Detektor in mV', fontsize = 18)
    ax.set_xbound(378, 650)
    ax.set_ybound(0.24, 4)

    #legend
    ax.legend(fontsize = 'large', loc = 'upper left')

    #fig.set_size_inches((8,6))
    plt.tight_layout()
    plt.subplots_adjust(left = 0.13, right = 0.99, bottom = 0.12, top = 0.99)

def plot_derivative():
    df = get_dataframe()
    temp_u, dvdt_u = dydx(unp.uarray(df['abs_temp'], df['temp_err']), unp.uarray(df['voltage'], df['voltage_err']))
    temp = unp.nominal_values(temp_u)
    temp_err = unp.std_devs(temp_u)
    dvdt = unp.nominal_values(dvdt_u)
    dvdt_err = unp.std_devs(dvdt_u)

    sigma2, sigma2_err, t2, t2_err = get_fit_params()

    x_fit = np.linspace(min(temp)-100, max(temp)+100, 800)
    y_fit = 4*sigma2*x_fit**3

    fig, ax = plt.subplots()
    ax.errorbar(temp, dvdt, xerr = temp_err, yerr = dvdt_err, fmt = 'b.', label = 'Datenpunkte', capsize = 2)
    ax.plot(x_fit, y_fit, 'r-', label = "$y=3*\log(T)+\log(4\sigma')$")

    #logarithmic scale
    ax.set_yscale('log')
    ax.set_xscale('log')

    #plot appearence
    ax.tick_params(labelsize = 16)
    
    #labels
    ax.set_xlabel('Temperautur in K', fontsize = 18)
    ax.set_ylabel(r'dU/dT in mV/K', fontsize = 18)
    ax.set_xbound(385, 640)
    ax.set_ybound(0.0046, 0.038)

    #legend
    ax.legend(fontsize = 'x-large')

    #fig.set_size_inches((8,6))
    plt.tight_layout()
    plt.subplots_adjust(left = 0.16, right = 0.99, bottom = 0.12, top = 0.99)

def plot_derivative_fit():
    df = get_dataframe()
    temp_l_u, dvdt_l_u = dydx(unp.uarray(df['abs_temp'], df['temp_err']), unp.uarray(df['voltage'], df['voltage_err']))
    temp_l = [unp.nominal_values(log(temp_l_u[i])) for i in range(len(temp_l_u))]
    temp_l_err = [unp.std_devs(log(temp_l_u[i])) for i in range(len(temp_l_u))]
    dvdt_l = [unp.nominal_values(log(dvdt_l_u[i])) for i in range(len(dvdt_l_u))]
    dvdt_l_err = [unp.std_devs(log(dvdt_l_u[i])) for i in range(len(dvdt_l_u))]

    output = fit_odr(linear, temp_l, dvdt_l, temp_l_err, dvdt_l_err, [3,1])

    x_fit = np.linspace(min(temp_l)-100, max(temp_l)+100, 800)
    #x_fit = np.linspace(min(temp_l), max(temp_l), 800)
    y_fit = linear(output.beta, x_fit)
    #y_fit_theoretical = 3.3*x_fit-25

    fig, ax = plt.subplots()
    ax.errorbar(temp_l, dvdt_l, xerr = temp_l_err, yerr = dvdt_l_err, fmt = 'b.', label = 'Datenpunkte', capsize = 2)
    ax.plot(x_fit, y_fit, 'r-', label = "$y=m\,\log(T)+const$")
    #ax.plot(x_fit, y_fit_theoretical, 'y-', label = "$y=3\,\log(T)+\log(4\sigma')$")

    fmtr = ShorthandFormatter()
    n_str = fmtr.format('{0:.1u}', ufloat(output.beta[0], output.sd_beta[0]))
    const_str = fmtr.format('{0:.1u}', ufloat(output.beta[1], output.sd_beta[1]))
    ax.plot([],[], ' ', label = "mit $m={}$ und $const={}$".format(n_str, const_str))
    #plot appearence
    ax.tick_params(labelsize = 16)
    
    #labels
    ax.set_xlabel(r'$\log(T)$ in K', fontsize = 18)
    ax.set_ylabel(r'$\log(dU/dT)$ in mV/K', fontsize = 18)
    ax.set_xbound(np.log(385), np.log(640))
    ax.set_ybound(np.log(0.0046), np.log(0.038))

    #legend
    ax.legend(fontsize = 'x-large')

    #fig.set_size_inches((8,6))
    plt.tight_layout()
    plt.subplots_adjust(left = 0.16, right = 0.99, bottom = 0.16, top = 0.99)


if __name__ == "__main__":
    #plot_linear()
    #plot_log()
    #plot_log_2()
    #plot_derivative()
    plot_derivative_fit()
    plt.show()
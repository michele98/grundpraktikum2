from uncertainties import ufloat
import numpy as np
from uncertainties.umath import *

sigma2 = ufloat(2.42*10**(-11), 0.014*10**(-11))
r1 = ufloat(19.3, 0.05)/2000
r2 = ufloat(25.1, 0.05)/2000
s = ufloat(0.03,0.01)
d = ufloat(84, 2)/1000

sigma = 2*sigma2/((np.pi*r1*r2)**2*s*(1-cos(r2/d)))
print (sigma)
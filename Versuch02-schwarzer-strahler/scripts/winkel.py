#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare

import sys
sys.path.insert(0,"../../")
from utils import ShorthandFormatter, fit_odr, relu

def f(B, x):
    return B[0]*np.cos(x)

def get_data():
    xls = pd.ExcelFile("../ofen.xlsx")
    df = pd.read_excel(xls, sheet_name = 'winkel')
    return df

def compute_r_real(r,h,scale,angle_offset,theta):
    theta = abs(theta+angle_offset)
    t1 = np.sqrt(1-(np.tan(theta)*h/(2*r))**2)*h*np.tan(theta)/(4*r)
    t2 = np.arcsin(h*np.tan(theta)/(2*r))/2
    return scale*np.cos(theta)*(np.pi/4-t1-t2)

def compute_r_real_fit(B,theta):
    theta = abs(theta+B[3])
    t1 = np.sqrt(1-(np.tan(theta)*B[1]/(2*B[0]))**2)*B[1]*np.tan(theta)/(4*B[0])
    t2 = np.arcsin(B[1]*np.tan(theta)/(2*B[0]))/2
    return B[2]*np.cos(theta)*(np.pi/4-t1-t2)

def compute_2(B,theta): #h/2r = B[0]; J0 = B[1]
    theta = abs(theta)
    t1 = B[0]*2*np.tan(theta)*np.sqrt(1-(np.tan(theta)*B[0])**2)/np.pi
    t2 = 2*np.arcsin(B[0]*np.tan(theta))/np.pi
    return B[1]*np.cos(theta)*(1-t1-t2)

def compute_3(B,theta):
    theta = abs(theta)
    b = 0.47
    t1 = np.sqrt(1-(np.tan(theta)*b)**2)*b*np.tan(theta)/2
    t2 = np.arcsin(b*np.tan(theta))/2
    return B[0]*np.cos(theta)*(np.pi/4-t1-t2)

def plot():
    df = get_data()
    #plt.rc('grid', linewidth = 0.5, linestyle = '--')

    fig, ax = plt.subplots()
    ax = plt.subplot(111, projection = 'polar')

    theta = (df['angle']-90)*np.pi/180
    theta_err = (df['angle_err'])*np.pi/180
    r = df['voltage']
    r_err = df['voltage_err']

    #plotting datapoints
    ax.errorbar(theta[:10], r[:10], xerr = theta_err[:10], yerr = r_err[:10], fmt = 'bo', alpha = 0.5, label = 'Messreihe 1')
    ax.errorbar(theta[10:27], r[10:27], xerr = theta_err[10:27], yerr = r_err[10:27], fmt = 'ro', alpha = 0.5, label = 'Messreihe 2')
    ax.errorbar(theta[27:], r[27:], xerr = theta_err[27:], yerr = r_err[27:], fmt = 'go', alpha = 0.5, label = 'Messreihe 3')

    #computing fit
    theta_fit = np.linspace(-180,180,360)*np.pi/180
    output_ideal = fit_odr(f, theta, r, theta_err, r_err, [3])
    #output_real = fit_odr(compute_r_real_fit, theta, r, theta_err, r_err, [1,1,1,0])
    output_real = fit_odr(compute_2, theta, r, theta_err, r_err, [0,0])
    #plotting fit
    #ax.plot(theta_fit, relu(f(output_ideal.beta, theta_fit)), 'k--', label = r'Ideale Kurve: $r(\theta) = r_0 \cos(\theta)$', linewidth = 1)
    ax.plot(theta_fit, relu(r[0]*np.cos(theta_fit)), 'k--', label = r"Ideale Kurve durch Lambertsches Gesetz: $U(\theta) = U_0 \cos(\theta)$", linewidth = 1)
    #curve_str = r"$r(\theta) = 4 \alpha R^2\cos(\theta)[\frac{\pi}{4}-\frac{h}{4R}\tan(\theta)\sqrt{1-(\frac{h}{2R}\tan(\theta))^2}-\frac{1}{2}\arcsin(\frac{h}{2R}\tan(\theta)]$"
    curve_str = r"$U(\theta) = U_0\,\cos(\theta)\,\left[\,1 - \frac{2\alpha}{\pi}\,\tan|\theta|\sqrt{1-(\alpha\,\tan|\theta|)^2} - \frac{2}{\pi}\,\arcsin(\alpha\,\tan|\theta|)\,\right]$"
    #ax.plot(theta_fit, relu(compute_r_real_fit(output_real.beta, theta_fit)), 'k-', label = r'Korrigierte Kurve: {}'.format(curve_str), linewidth = 1)
    ax.plot(theta_fit, relu(compute_2(output_real.beta, theta_fit)), 'k-', label = r'Korrigierte Kurve: {}'.format(curve_str), linewidth = 1)
    #c1, p1 = chisquare(r, compute_r_real_fit(output_real.beta, theta))
    c1, p1 = chisquare(r, compute_2(output_real.beta, theta))
    c2, p2 = chisquare(r, f(output_ideal.beta, theta))
    print(unp.uarray(output_real.beta, output_real.sd_beta))
    print(c1, p1)
    print(c2, p2)
    #print(output_real.beta[3]*180/np.pi)
    
    #plot appearence
    ax.set_rmax(4.2)
    ax.set_thetamin(-90)
    ax.set_thetamax(90)
    ax.set_theta_zero_location('N')
    ax.set_rticks([1,2,3,4])
    ax.tick_params(axis = 'x', labelsize = 18, pad = 10)
    ax.tick_params(axis = 'y', labelsize = 18)
    ax.set_rlabel_position(90)
    ax.grid(linestyle = '--', linewidth = 0.5)
    #ax.set_position([0.1,-0.45,0.8,2])
    ax.set_position([0.05,-0.42, 0.9, 1.9])
    ax.set_xlabel('Spannung U am Detektor in mV', fontsize = 18, labelpad = -130)
    #ax.legend(bbox_to_anchor=(0.75, 0.35), loc='center right', borderaxespad=0., fontsize = 'x-large')
    ax.legend(bbox_to_anchor=(1, 0.35), loc='center right', borderaxespad=0., fontsize = 'x-large')
    #ax.legend(fontsize = 'x-large')

    fig.set_size_inches((10,6))
    #plt.subplots_adjust(bottom = 0, top = 1, right = 1, left = 0)

if __name__ == "__main__":
    plot()
    plt.show()
import matplotlib.pyplot as plt
import numpy as np

t = np.linspace(0, 2*np.pi, 500)
a = 2
b = 3
d = 1
x1 = a*np.cos(t)+d/2
y1 = b*np.sin(t)
x2 = x1-d
y2 = y1

fig, ax = plt.subplots()
ax.plot(x1,y1,'b-', linewidth = 1)
ax.plot(x2,y2,'b-', linewidth = 1)
x_fill_1 = np.linspace(min(x1), max(x1), 200)
y_fill_1 = b*np.sqrt(1-((x_fill_1-d/2)/a)**2)
x_fill_2 = np.linspace(min(x2), max(x2), 200)
y_fill_2 = b*np.sqrt(1-((x_fill_2+d/2)/a)**2)
ax.fill_between(x_fill_1, y_fill_1, color = 'b', alpha = 0.1)
ax.fill_between(x_fill_1, -y_fill_1, color = 'b', alpha = 0.1)
ax.fill_between(x_fill_2, y_fill_2, color = 'b', alpha = 0.1)
ax.fill_between(x_fill_2, -y_fill_2, color = 'b', alpha = 0.1)
ax.set_aspect('equal')

ax.spines['left'].set_position('zero')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

ax.tick_params(labelsize = 16)

fig.set_size_inches((7,7))
plt.subplots_adjust(top=0.99, bottom=0.01, left = 0.01, right=0.99)
plt.show()
#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu

def linear(B,x):
    return B[0]*x+B[1]

def linear2(B,x):
    return B[0]*x

def get_dataframe(sheet):
    xls = pd.ExcelFile("./daten_07.xlsx")
    return pd.read_excel(xls, sheet_name = sheet)

def get_data_reflection():
    df = get_dataframe('reflection')
    alpha = np.array(df['alpha'])
    beta = np.array(df['beta'])
    alpha_err = np.array(df['alpha_err'])
    beta_err = np.array(df['beta_err'])
    return alpha, beta, alpha_err, beta_err

def plot_reflection():
    alpha, beta, alpha_err, beta_err = get_data_reflection()
    fmtr = ShorthandFormatter()

    fig, ax = plt.subplots()
    output = fit_odr(linear, alpha, beta, alpha_err, beta_err, [1,1])
    output2 = fit_odr(linear2, alpha, beta, alpha_err, beta_err, [1])
    x_fit = np.linspace(20,65, 100)

    ax.errorbar(alpha, beta, xerr = alpha_err, yerr = beta_err, fmt= 'b.', capsize = 2, label = 'Messpunkte')
    #ax.plot(x_fit, linear(output.beta, x_fit), 'g-', label = r'linearer Fit mit $y=ax+b$')
    #ax.plot([], [], ' ', label = fmtr.format(r'$a={:.1u}$, $b={:.1u}^\circ$', ufloat(output.beta[0], output.sd_beta[0]), ufloat(output.beta[1], output.sd_beta[1])))
    ax.plot(x_fit, linear2(output2.beta, x_fit), 'r-', label = r'linearer Fit mit $y=Ax$')
    ax.plot([], [], ' ', label = fmtr.format(r'A={:.1u}', ufloat(output2.beta[0], output2.sd_beta[0])))

    ax.set_ybound(28,63)
    ax.set_xbound(26,63)

    ax.tick_params(labelsize = 18)
    ax.set_xlabel(r'$\alpha$ in Grad', fontsize = 18)
    ax.set_ylabel(r'$\beta$ in Grad', fontsize = 18)
    ax.legend(loc = 'upper left', fontsize = 14)
    plt.subplots_adjust(left = 0.12, bottom = 0.14, right = 0.99, top = 0.99)

def get_data_michaelson():
    df = get_dataframe('michaelson')
    x = np.array(df['x'])
    i = np.array(df['i'])
    x_err = np.array(df['x_err'])
    i_err = np.array(df['i_err'])
    return x, i, x_err, i_err

def cosf(B,x):
    return B[0]*np.cos(B[1]*x+B[2])+B[3]

def plot_michaelson():
    x, i, x_err, i_err = get_data_michaelson()
    fmtr = ShorthandFormatter()

    output = fit_odr(cosf, x, np.sqrt(i), x_err, i_err/(2*np.sqrt(i)), [0.2,4.5,0,0.4])
    output.pprint()
    fig, ax = plt.subplots()
    x_fit = np.linspace(min(x), max(x), 200)

    ax.errorbar(x, i, xerr = x_err, yerr = i_err, fmt= 'b.', capsize = 2, label = 'Messpunkte')
    ax.plot(x_fit, cosf(output.beta, x_fit)**2, 'r-')
    #ax.set_ybound(28,63)
    #ax.set_xbound(26,63)

    ax.tick_params(labelsize = 18)
    ax.set_xlabel('Auslenkung x in cm', fontsize = 18)
    ax.set_ylabel(u'Intensität am Messgerät in mA', fontsize = 18)
    ax.legend(loc = 'upper right', fontsize = 14)
    plt.subplots_adjust(left = 0.14, bottom = 0.14, right = 0.99, top = 0.99)

if __name__=='__main__':
    #plot_reflection()
    plot_michaelson()
    plt.show()
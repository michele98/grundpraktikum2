#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare
#import scipy.interpolate as interp

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu

def get_dataframe(sheet):
    xls = pd.ExcelFile("./daten_05.xlsx")
    return pd.read_excel(xls, sheet_name = sheet)

def weighted_average(values, errors):
    weights = np.array([1/error**2 for error in errors])
    vals = np.array(values)
    return sum(vals*weights)/sum(weights), np.sqrt(1/sum(weights))

def average(values):
    avg = sum(values)/len(values)
    return avg, np.sqrt(sum([(value-avg)**2 for value in values])/(len(values)-1))

def quadratic_0(B,x):
    return B[0]*x*x

def linear(B,x):
    return B[0]*x

def linear_2(B,x):
    return B[0]*x+B[1]

def linear_2_c(x,a,b):
    return a*x+b

def get_data_schraube():
    df = get_dataframe('schraube')
    begin = np.array(df['begin in mm'])
    begin_err = np.array(df['begin_err'])
    end = np.array(df['end in mm'])
    end_err = np.array(df['end_err'])
    rings = np.array(df['rings'])
    rings_err = np.array(df['rings_err'])
    return unp.uarray(begin, begin_err), unp.uarray(end, end_err), unp.uarray(rings, rings_err)

def plot_schraube():
    begin, end, rings = get_data_schraube()
    wavelength = 632.8*(10**(-6)) #in mm
    d = rings*wavelength/2 #durch den Interferometer gemessene Auslenkung
    tr = (end-begin)/d #translation durch Hebelsystem
    fig, ax = plt.subplots()
    ax2 = ax.twinx()
    ax.errorbar(unp.nominal_values(begin), unp.nominal_values(end-begin), xerr = unp.std_devs(begin), yerr = unp.std_devs(end-begin), fmt='ro', capsize = 2, label = 'Verstellung', linewidth = 1)
    ax2.errorbar(unp.nominal_values(begin), unp.nominal_values(tr), xerr = unp.std_devs(begin), yerr = unp.std_devs(tr), fmt='bo', capsize = 2, label = u'Übersetzung', linewidth = 1)
    
    
    ax.set_xlabel('Anfangsposition in mm', fontsize = 18)
    ax.set_ylabel('Verstellweg in mm', fontsize = 18)
    ax2.set_ylabel('Übersetzung', fontsize = 18)

    ax.tick_params(labelsize = 16)
    ax2.tick_params(labelsize = 16)

    handles, labels = ax.get_legend_handles_labels()
    handles2, labels2 = ax2.get_legend_handles_labels()

    ax.legend(handles + handles2, labels + labels2, loc = 0)
    plt.tight_layout()

def plot_schraube_2():
    begin, end, rings = get_data_schraube()
    fmtr = ShorthandFormatter()

    wavelength = 632.8*(10**(-6)) #in mm
    d = rings*wavelength/2 #durch den Interferometer gemessene Auslenkung
    tr = (end-begin)/d #translation durch Hebelsystem
    fig, ax = plt.subplots()
    
    tr_avg = weighted_average(unp.nominal_values(tr), unp.std_devs(tr))

    ax.errorbar(unp.nominal_values(begin), unp.nominal_values(tr), xerr = unp.std_devs(begin), yerr = unp.std_devs(tr), fmt='bo', capsize = 2, label = u'Messwerte', linewidth = 1)
    ax.plot([], [], ' ', label = fmtr.format(r'$u = {:.1u}$', ufloat(tr_avg[0], tr_avg[1])))
    ax.plot([-1,11], [tr_avg[0],tr_avg[0]], 'r-', label = u'gewichteter Mittelwert')
    ax.fill_between([-1,11], [tr_avg[0]-tr_avg[1],tr_avg[0]-tr_avg[1]], [tr_avg[0]+tr_avg[1],tr_avg[0]+tr_avg[1]], color = 'r', alpha = 0.16)
    #ax.plot([-1,11], [tr_avg[0]-tr_avg[1],tr_avg[0]-tr_avg[1]], 'r--')
    #ax.plot([-1,11], [tr_avg[0]+tr_avg[1],tr_avg[0]+tr_avg[1]], 'r--')

    ax.set_xbound(-0.5,10.5)
    ax.set_ybound(9.7,14)

    ax.set_xlabel('Anfangsposition in mm', fontsize = 18)
    ax.set_ylabel(u'Übersetzung $u$', fontsize = 18)
    ax.tick_params(labelsize = 16)

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(reversed(handles), reversed(labels), fontsize = 'xx-large')
    #ax.legend(fontsize = 'xx-large')

    plt.subplots_adjust(left=0.12, bottom = 0.14, right = 0.99, top = 0.97)

def get_data_luft():
    df = get_dataframe('luft')
    p_before = unp.uarray(df['p_before in mbar'], df['p_before_err'])
    p_after = unp.uarray(df['p_after in mbar'], df['p_after_err'])
    rings = unp.uarray(df['rings'], df['rings_err'])

    remove = [4,5]
    p_before = np.array([p_before[i] for i in range(len(p_before)) if i not in remove])
    p_after = np.array([p_after[i] for i in range(len(p_after)) if i not in remove])
    rings = np.array([rings[i] for i in range(len(rings)) if i not in remove])

    return p_before, p_after, rings

def plot_luft():
    p_before, p_after, rings = get_data_luft()
    wavelength = 632.8*(10**(-6))
    p_atm = ufloat(946.37, 0.19)
    p_before = p_atm-p_before
    p_after = p_atm-p_after
    l = ufloat(44.9, 0.05)-2*ufloat(3.2, 0.1)
    fig, ax = plt.subplots()

    fmtr = ShorthandFormatter()

    p = (p_before+p_after)/2
    a = wavelength*rings/(2*l*(p_after-p_before))
    n = a*p
    a_avg = weighted_average(unp.nominal_values(a), unp.std_devs(a))

    mult = 1000000

    ax.errorbar(unp.nominal_values(p), mult*unp.nominal_values(a), xerr = unp.std_devs(p), yerr = mult*unp.std_devs(a), fmt = 'b.', capsize = 2, label = 'Messpunkte', linewidth = 1)

    x_fit = np.array([0,mult])
    ax.plot(x_fit, [mult*a_avg[0], mult*a_avg[0]], 'r-', linewidth = 1, label = u'Mittelwert für a')
    ax.plot([],[], ' ', label = fmtr.format(r'$a_m = {}$ mbar$^{{-1}}$', ufloat(a_avg[0], a_avg[1])))
    ax.fill_between(x_fit, mult*np.array([a_avg[0]-a_avg[1],a_avg[0]-a_avg[1]]), mult*np.array([a_avg[0]+a_avg[1],a_avg[0]+a_avg[1]]), color = 'r', alpha = 0.16)

    ax.set_xbound(230, 820)
    ax.set_ybound(0.14,0.36)

    ax.set_xlabel(r'Druck $p$ in mbar', fontsize = 18)
    ax.set_ylabel(r'$a \cdot 10^6$ in mbar$^{-1}$', fontsize = 18)
    ax.tick_params(labelsize = 16)

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(reversed(handles), reversed(labels), loc = 'lower left', fontsize = 'x-large')

    plt.subplots_adjust(left=0.16, bottom = 0.14, right = 0.99, top = 0.97)

def plot_luft_2():
    p_before, p_after, rings = get_data_luft()
    wavelength = 632.8*(10**(-6))
    p_atm = ufloat(946.37, 0.19)
    p_atm = 946.37
    p_before = p_atm-p_before
    p_after = p_atm-p_after
    l = ufloat(44.9, 0.05)-2*ufloat(3.2, 0.1)
    fig, ax = plt.subplots()

    fmtr = ShorthandFormatter()

    p = (p_before+p_after)/2
    a = wavelength*rings/(2*l*(p_after-p_before))
    n = a*p
    a_avg = weighted_average(unp.nominal_values(a), unp.std_devs(a))

    output = fit_odr(linear_2, unp.nominal_values(p)-500, unp.nominal_values(n), unp.std_devs(p), unp.std_devs(n), [1,1])
    popt, cov = curve_fit(linear_2_c, unp.nominal_values(p), unp.nominal_values(n), sigma = unp.std_devs(n), absolute_sigma=True)
    a_fit = ufloat(output.beta[0], output.sd_beta[0])
    #a_fit = ufloat(popt[0], np.sqrt(cov[0][0]))

    mult = 1000

    #ax.errorbar(unp.nominal_values(p), unp.nominal_values(a), xerr = unp.std_devs(p), yerr = unp.std_devs(a), fmt = 'b.', capsize = 2)
    ax.errorbar(unp.nominal_values(p), mult*unp.nominal_values(n), xerr = unp.std_devs(p), yerr = mult*unp.std_devs(n), fmt = 'b.', capsize = 2, label = 'Messpunkte', linewidth = 1)
    x_fit = np.array([0,mult])
    ax.plot(x_fit, mult*x_fit*a_avg[0], 'r-', linewidth = 1, label = '$y=a_mx$')
    ax.plot([],[], ' ', label = fmtr.format(r'$n_{{atm}} = {}$', 1+1013*ufloat(a_avg[0], a_avg[1])))
    ax.fill_between(x_fit, mult*linear([a_avg[0]-a_avg[1]], x_fit), mult*linear([a_avg[0]+a_avg[1]], x_fit), color = 'r', alpha = 0.16)

    ax.plot(x_fit, mult*linear([a_fit.nominal_value], x_fit), 'g-', linewidth = 1, label = '$y=a_{fit}x$')
    ax.plot([],[], ' ', label = fmtr.format(r'$n_{{atm2}} = {:.1u}$', 1+1013*ufloat(a_fit.nominal_value, a_fit.std_dev)))
    ax.fill_between(x_fit, mult*linear([a_fit.nominal_value-a_fit.std_dev], x_fit), mult*linear([a_fit.nominal_value+a_fit.std_dev], x_fit), color = 'g', alpha = 0.16)

    #ax.plot(x_fit, mult*linear_2(output.beta, x_fit), 'y-', linewidth = 1, label = 'Fit mit $y=ax+b$')
    ax.plot([],[], ' ', label = fmtr.format(r'ODR $a = {:.1u}$, $b={:.1u}$', ufloat(output.beta[0], output.sd_beta[0]), ufloat(output.beta[1], output.sd_beta[1])))
    ax.plot([],[], ' ', label = fmtr.format(r'c_fit $a = {:.1u}$, $b={:.1u}$', ufloat(popt[0], np.sqrt(cov[0][0])), ufloat(popt[1], np.sqrt(cov[1][1]))))

    ax.set_xbound(230, 820)
    ax.set_ybound(0.05,0.28)

    ax.set_xlabel(r'Druck $p$ in mbar', fontsize = 18)
    ax.set_ylabel(r'$(n-1) \cdot 10^3$', fontsize = 18)
    ax.tick_params(labelsize = 16)

    handles, labels = ax.get_legend_handles_labels()
    #ax.legend(reversed(handles), reversed(labels), loc = 'upper left', fontsize = 'xx-large')
    ax.legend(loc = 'upper left', fontsize = 'x-large')
    
    plt.subplots_adjust(left=0.16, bottom = 0.14, right = 0.99, top = 0.97)

def get_data_glas():
    df = get_dataframe('glas')
    angle = np.array(df['angle'])
    angle_err = np.array(df['angle_err'])
    rings = np.array(df['rings'])
    rings_err = np.array(df['rings_err'])
    return unp.uarray(angle, angle_err), unp.uarray(rings, rings_err)

wavelength = 632.8*(10**(-6))
d = ufloat(5.65,0.05)


def plot_glas():
    angle, rings = get_data_glas()
    angle_rad = angle*np.pi/180
    d = 5.65
    n = 1/(1-wavelength*rings/(d*angle_rad*angle_rad))
    n_avg, n_avg_err = weighted_average(unp.nominal_values(n), unp.std_devs(n))

    fmtr = ShorthandFormatter()

    fig, ax = plt.subplots()
    ax.errorbar(unp.nominal_values(angle), unp.nominal_values(n), xerr = unp.std_devs(angle), yerr = unp.std_devs(n), fmt = 'bo', capsize = 2, label = 'Messwerte', linewidth = 1, markersize = 4)
    x_avg = [-13,13]
    ax.plot(x_avg, [n_avg,n_avg], 'r-', label = u'Mittelwert für n')
    ax.plot([],[], ' ', label =fmtr.format(r'mit n$_m$={0:.1u}', ufloat(n_avg, n_avg_err)))
    ax.fill_between(x_avg, [n_avg-n_avg_err,n_avg-n_avg_err], [n_avg+n_avg_err,n_avg+n_avg_err], color = 'r', alpha = 0.16)

    ax.set_xlabel('Winkel in Grad', fontsize = 18)
    ax.set_ylabel('Brechungsindex n', fontsize = 18)
    ax.tick_params(labelsize = 18)
    ax.set_xbound(-10.3, 11)
    ax.set_ybound(1.5, 2.21)

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(reversed(handles), reversed(labels), fontsize='x-large', bbox_to_anchor=(0.995,0.99), borderaxespad=0.)

    plt.subplots_adjust(left = 0.13, right = 0.99, top = 0.99, bottom = 0.13)

def plot_glas_2():
    angle, rings = get_data_glas()
    fmtr = ShorthandFormatter()

    angle_rad = angle*np.pi/180

    output = fit_odr(quadratic_0, unp.nominal_values(angle), unp.nominal_values(rings), unp.std_devs(angle), unp.std_devs(rings), [1])
    output_rad = fit_odr(quadratic_0, unp.nominal_values(angle_rad), unp.nominal_values(rings), unp.std_devs(angle_rad), unp.std_devs(rings), [1])

    A = ufloat(output_rad.beta[0], output_rad.sd_beta[0])
    n = 1/(1-wavelength*A/d)

    x_fit = np.linspace(-11,11,100)

    fig, ax = plt.subplots()
    ax.errorbar(unp.nominal_values(angle), unp.nominal_values(rings), xerr = unp.std_devs(angle), yerr = unp.std_devs(rings), fmt = 'b.', capsize = 2, label = 'Messwerte', linewidth = 1)
    ax.plot(x_fit, quadratic_0(output.beta, x_fit), 'r-', linewidth=1, label = r'Fit mit $y=Ax^2$')
    ax.plot([], [], ' ', label = fmtr.format(r'$A={:.1u},\; n={:.1u}$', ufloat(output_rad.beta, output_rad.sd_beta), n))

    ax.set_xlabel('Winkel in Grad', fontsize = 18)
    ax.set_ylabel('Anzahl der Ringe', fontsize = 18)
    ax.tick_params(labelsize = 18)
    ax.set_xbound(-10.3, 11)
    ax.set_ybound(-1, 125)

    ax.legend(fontsize='x-large')

    plt.subplots_adjust(left = 0.15, right = 0.99, top = 0.99, bottom = 0.13)

def plot_glas_3():
    angle, rings = get_data_glas()
    fmtr = ShorthandFormatter()

    angle_rad = angle*np.pi/180

    output = fit_odr(linear, unp.nominal_values(angle**2), unp.nominal_values(rings), unp.std_devs(angle**2), unp.std_devs(rings), [1])
    output_rad = fit_odr(linear, unp.nominal_values(angle_rad**2), unp.nominal_values(rings), unp.std_devs(angle_rad**2), unp.std_devs(rings), [1])

    A = ufloat(output_rad.beta[0], output_rad.sd_beta[0])
    n = 1/(1-wavelength*A/d)

    x_fit = np.linspace(-1,121,100)

    fig, ax = plt.subplots()
    ax.errorbar(unp.nominal_values(angle**2), unp.nominal_values(rings), xerr = unp.std_devs(angle**2), yerr = unp.std_devs(rings), fmt = 'b.', capsize = 2, label = 'Messwerte', linewidth = 1)
    ax.plot(x_fit, linear(output.beta, x_fit), 'r-', linewidth=1, label = r'Fit mit $y=Ax$')
    ax.plot([], [], ' ', label = fmtr.format(r'$A={:.1u}$', ufloat(output_rad.beta, output_rad.sd_beta)))
    ax.plot([], [], ' ', label = fmtr.format(r'$n={:.1u}$', n))

    ax.set_xlabel(r'$\alpha^2$ in Grad$^2$', fontsize = 18)
    ax.set_ylabel('Anzahl der Ringe', fontsize = 18)
    ax.tick_params(labelsize = 18)
    ax.set_xbound(0, 121)
    ax.set_ybound(0, 125)

    ax.legend(loc = 'upper left', fontsize='x-large')

    plt.subplots_adjust(left = 0.15, right = 0.99, top = 0.99, bottom = 0.13)
if __name__ == '__main__':
    #plot_schraube_2()
    #plot_schraube()
    plot_luft()
    plot_luft_2()
    #plot_glas()
    #plot_glas_2()
    #plot_glas_3()
    plt.show()
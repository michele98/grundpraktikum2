#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare
from scipy.constants import *
#import scipy.interpolate as interp

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu

def get_dataframe(sheet):
    xls = pd.ExcelFile("./daten_06.xlsx")
    return pd.read_excel(xls, sheet_name = sheet)

def linear_2(x,a,b):
    return a*x+b

def linear(B,x):
    return B[0]*x+B[1]

def linear1(B,x):
    return B[0]*x

def quadratic(B,x):
    return B[0]*x*x+B[1]*x+B[2]

def quadratic2(B,x):
    return B[0]*x*x+B[1]*x

def weighted_average(values, errors):
    weights = np.array([1/error**2 for error in errors])
    vals = np.array(values)
    return sum(vals*weights)/sum(weights), np.sqrt(1/sum(weights))

def average(values):
    avg = sum(values)/len(values)
    return avg, np.sqrt(sum([(value-avg)**2 for value in values])/(len(values)-1))

def get_data_led_red():
    df = get_dataframe('led_rot')
    u = np.array(df['u in V'])
    i = np.array(df['i in mA'])
    p = np.array(df['p in uW'])-4
    u_err = np.array(df['u_err'])
    i_err = np.array(df['i_err'])
    p_err = np.array(df['p_err'])
    return u, i, p, u_err, i_err, p_err

def get_data_led_blue():
    df = get_dataframe('led_blau')
    u = np.array(df['u in V'])
    i = np.array(df['i in mA'])
    p = np.array(df['p in mW'])
    u_err = np.array(df['u_err'])
    i_err = np.array(df['i_err'])
    p_err = np.array(df['p_err'])
    return u, i, p, u_err, i_err, p_err

def plot_led_red_ui():
    u, i, p, u_err, i_err, p_err = get_data_led_red()
    fig, ax = plt.subplots()

    output_inv = fit_odr(linear, i, u, i_err, u_err, [1,1])
    output = fit_odr(linear, u, i, u_err, i_err, [1,1])
    
    x_fit = np.array([1,3])

    fmtr = ShorthandFormatter()
    u_d = ufloat(output_inv.beta[1], output_inv.sd_beta[1])
    #u_d = -ufloat(output.beta[1], output.sd_beta[1])/ufloat(output.beta[0], output.sd_beta[0])
    u_d_str = fmtr.format('{0:.1u}', u_d)

    ax.errorbar(u, i, xerr = u_err, yerr = i_err, fmt='b.', capsize = 2, label = 'Messpunkte')
    ax.plot(x_fit,linear(output.beta,x_fit),'r-', label = 'linearer Fit $y=ax+b$')
    ax.plot([],[],' ', label = fmtr.format(r'a={:.1u} mA/V, b={:.1u} mA', ufloat(output.beta[0], output.sd_beta[0]), ufloat(output.beta[1], output.sd_beta[1])))
    #ax.errorbar([output.beta[1]],[0], xerr = output.sd_beta[1], fmt = 'k.', capsize = 2, label = r'$U_d={}\,V$'.format(u_d_str))
    ax.plot([u_d.nominal_value, u_d.nominal_value], [-1,45], 'k--', linewidth = 1, label = r'$U_d={}\,V$'.format(u_d_str))
    ax.fill_between([u_d.nominal_value-u_d.std_dev, u_d.nominal_value+u_d.std_dev], -1, 45, color = 'k', alpha = 0.1)

    ax.legend(fontsize = 'x-large', loc = 'upper left')
    ax.tick_params(labelsize = 18)
    ax.set_xlabel('Spannung U in V', fontsize = 18)
    ax.set_ylabel('Strom I in mA', fontsize = 18)

    ax.set_ybound(0,42)
    ax.set_xbound(1.5,2.5)
    plt.subplots_adjust(left = 0.12, bottom = 0.14, right = 0.99, top = 0.99)

def plot_led_red_ip():
    u, i, p, u_err, i_err, p_err = get_data_led_red()
    df = get_dataframe('led_rot')
    fig, ax = plt.subplots()
    fmtr = ShorthandFormatter()
    p_corr = unp.uarray(p,p_err)/calculate_proportion_red()
    p/=calculate_proportion_red().nominal_value
    
    p_i = u*i
    p_i_err = np.sqrt((i*u_err)**2+(u*i_err)**2)
    nu = c/(718*10**(-9))

    #n_ext = p_corr*e/(unp.uarray(i,i_err)*h*nu)
    #n_k = p_corr/unp.uarray(p_i, p_i_err)
    n_ext = unp.uarray(df['n_ext'], df['n_ext_err'])
    n_k = unp.uarray(df['n_k'], df['n_k_err'])

    output = fit_odr(linear1, i, p, i_err, p_err, [1])
    #output_i = fit_odr(quadratic2, i, p_i, i_err, p_i_err, [1,1])
    x_fit = np.linspace(0,50,300)

    ax.errorbar(i, p, xerr = i_err, yerr = p_err, fmt='b.', capsize = 2, label = 'Messpunkte')
    ax.plot(x_fit, linear1(output.beta, x_fit), 'r-', label = 'linearer Fit $y=ax$')
    ax.plot([],[],' ', label = fmtr.format('a={:.1u} mW/A', ufloat(output.beta[0], output.sd_beta[0])))
    #ax.plot([],[], ' ', label = r'$\eta_{k} = $' + fmtr.format('{:.1u}', n_k[-1]))
    #ax.plot([],[], ' ', label = r'$\eta_{ext} = $' + fmtr.format('{:.1u}', n_ext[-1]))
    #ax.errorbar(i, p_i, xerr = i_err, yerr = p_i_err, fmt='g.', capsize = 2, label = 'Stromleistung')
    #ax.plot(x_fit,quadratic2(output_i.beta, x_fit), 'y-', label = 'linearer Fit $y=ax$')
    #ax.plot([],[],' ', label = fmtr.format('a={} mW/A', ufloat(output_i.beta[0], output_i.sd_beta[0])))

    ax.legend(fontsize = 'x-large', loc = 'upper left')
    ax.tick_params(labelsize = 18)
    ax.set_xlabel('Strom I in mA', fontsize = 18)
    ax.set_ylabel(r'Leistung am Detektor in $\mu W$', fontsize = 18)

    ax.set_ybound(0,102)
    ax.set_xbound(0,42)
    plt.subplots_adjust(left = 0.15, bottom = 0.14, right = 0.99, top = 0.99)

def calculate_proportion_red():
    a = ufloat(76.5, 0.1)
    b = ufloat(43.4, 0.1)
    c = ufloat(144.2, 0.1)
    d = ufloat(81.4, 0.1)
    return ufloat((0.5*(a/b+c/d)).nominal_value, abs((a/b-c/d).nominal_value))
    #return ufloat(1,0)

def plot_led_blue():
    u, i, p, u_err, i_err, p_err = get_data_led_blue()
    fig, ax = plt.subplots()
    fmtr = ShorthandFormatter()

    f = quadratic
    output = fit_odr(f, u, i, u_err, i_err, [1,1,1])
    output.pprint()
    last = 5
    output_lin = fit_odr(linear, i[-last:], u[-last:], i_err[-last:], u_err[-last:], [1,1])
    #print ([fmtr.format('{}', ufloat(output_lin.beta[i], output_lin.sd_beta[i])) for i in range(2)])
    u_d = ufloat(output_lin.beta[1], output_lin.sd_beta[1])
    output_lin = fit_odr(linear, u[-last:], i[-last:], u_err[-last:], i_err[-last:], [1,1])
    #print ([fmtr.format('{}', ufloat(output_lin.beta[i], output_lin.sd_beta[i])) for i in range(2)])
    
    x_fit = np.linspace(2,4,100)
    #u_d = -ufloat(output_lin.beta[1], output_lin.sd_beta[1])/ufloat(output_lin.beta[0], output_lin.sd_beta[0])
    u_d_str = fmtr.format('{0:.1u}', u_d)

    ax.errorbar(u, i, xerr = u_err, yerr = i_err, fmt='b.', capsize = 2, label = 'Messpunkte')
    ax.plot(x_fit,f(output.beta,x_fit),'r-', label = r'Parabelfit $y=Ax^2+Bx+C$')
    #ax.plot([],[], ' ', label = fmtr.format('mit A={}, B={} und C={}', ufloat(output.beta[0], output.sd_beta[0]), ufloat(output.beta[1], output.sd_beta[1]), ufloat(output.beta[2], output.sd_beta[2])))
    #ax.plot(x_fit,linear(output_lin.beta,x_fit),'g-', label = 'linearer Fit auf den letzten {} Datenpunkten'.format(last))
    ax.plot(x_fit,linear(output_lin.beta,x_fit),'g-', label = r'linearer Fit $y=ax+b$')
    ax.plot([],[], ' ', label = fmtr.format('a={}, b={}', ufloat(output_lin.beta[0], output_lin.sd_beta[0]), ufloat(output_lin.beta[1], output_lin.sd_beta[1])))
    ax.plot([u_d.nominal_value, u_d.nominal_value], [-1,45], 'k--', linewidth = 1, label = r'$U_d={}\,V$'.format(u_d_str))
    ax.fill_between([u_d.nominal_value-u_d.std_dev, u_d.nominal_value+u_d.std_dev], -1, 45, color = 'k', alpha = 0.1)

    ax.legend(fontsize = 'x-large')
    ax.tick_params(labelsize = 18)
    ax.set_xlabel('Spannung U in V', fontsize = 18)
    ax.set_ylabel('Strom I in mA', fontsize = 18)

    ax.set_ybound(0,42)
    ax.set_xbound(3,3.63)
    plt.subplots_adjust(left = 0.12, bottom = 0.14, right = 0.99, top = 0.99)

if __name__ == '__main__':
    plot_led_red_ui()
    plot_led_red_ip()
    plot_led_blue()
    plt.show()
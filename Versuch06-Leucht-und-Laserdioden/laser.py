#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare
from scipy.constants import *
#import scipy.interpolate as interp

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu

def get_dataframe(sheet):
    xls = pd.ExcelFile("./daten_06.xlsx")
    return pd.read_excel(xls, sheet_name = sheet)

def linear_2(x,a,b):
    return a*x+b

def linear(B,x):
    return B[0]*x+B[1]

def gaussian_2(x,A,s,xo):
    return A*np.exp(-(x-xo)**2/s)

def gaussian(B,x):
    return B[0]*np.exp(-((x-B[2])/B[1])**2/2)

def weighted_average(values, errors):
    weights = np.array([1/error**2 for error in errors])
    vals = np.array(values)
    return sum(vals*weights)/sum(weights), np.sqrt(1/sum(weights))

def average(values):
    avg = sum(values)/len(values)
    return avg, np.sqrt(sum([(value-avg)**2 for value in values])/(len(values)-1))

def get_data_laser():
    df = get_dataframe('laser')
    i = np.array(df['i in mA'])
    p = np.array(df['p in uW'])/1000
    i_err = np.array(df['i_err'])
    p_err = np.array(df['p_err'])/1000
    return i, p, i_err, p_err

def get_data_gitter():
    df = get_dataframe('gitter')
    x = np.array(df['x in cm'])-12.21635885
    p = np.array(df['p in uW'])-0.2
    x_err = np.array(df['x_err'])
    p_err = np.array(df['p_err'])
    return 10*x, p, 10*x_err, p_err

def plot_laser():
    i, p, i_err, p_err = get_data_laser()
    fig, ax = plt.subplots()
    fmtr = ShorthandFormatter()

    first = 8
    last = 8
    output_before = fit_odr(linear, i[:first], p[:first], i_err[:first], p_err[:first], [1,1])
    output_after = fit_odr(linear, i[-last:-2], p[-last:-2], i_err[-last:-2], p_err[-last:-2], [1,1])

    a = ufloat(output_before.beta[0], output_before.sd_beta[0])
    b = ufloat(output_before.beta[1], output_before.sd_beta[1])
    c = ufloat(output_after.beta[0], output_after.sd_beta[0])
    d = ufloat(output_after.beta[1], output_after.sd_beta[1])
    i_s = (d-b)/(a-c)

    x_fit_1 = np.linspace(-1,46,2)
    x_fit_2 = np.linspace(30,46,2)

    ax.errorbar(i[-last:-2], p[-last:-2], xerr = i_err[-last:-2], yerr = p_err[-last:-2], fmt = 'b.', capsize = 2, label = 'gefittete Messpunkte')
    ax.errorbar(i[:first], p[:first], xerr = i_err[:first], yerr = p_err[:first], fmt = 'b.', capsize = 2)
    ax.errorbar(i[first:-last], p[first:-last], xerr = i_err[first:-last], yerr = p_err[first:-last], fmt = '*', color = '#ff9c1b', capsize = 2, label = 'nicht gefittete Messpunkte')
    ax.errorbar(i[-2:], p[-2:], xerr = i_err[-2:], yerr = p_err[-2:], fmt = '*', color = '#ff9c1b', capsize = 2)

    ax.plot(x_fit_1, linear(output_before.beta, x_fit_1), 'g-', label = r'linearer Fit befor $I_s$')
    ax.plot([], [], ' ', label = fmtr.format(r'a={:.1u} mW/A; b={:.1u} $\mu$W', a*1000, b*1000))
    ax.plot(x_fit_2, linear(output_after.beta, x_fit_2), 'r-', label = r'linearer Fit nach $I_s$')
    ax.plot([], [], ' ', label = fmtr.format(r'a={:.1u} mW/A; b={:.1u} mW', c*1000, d))
    ax.plot([i_s.nominal_value, i_s.nominal_value], [-1,3], 'k--', linewidth = 1, label = fmtr.format(r'$I_s={0:.1u}$mA', i_s))

    ax.plot([-1,50], [0,0], 'k-', linewidth = 0.5)
    ax.plot([0,0], [-1,4], 'k-', linewidth = 0.5)
    ax.fill_between([i_s.nominal_value-i_s.std_dev, i_s.nominal_value+i_s.std_dev], -1, 4, color = 'k', alpha = 0.1)

    handles, labels = ax.get_legend_handles_labels()
    handles = [h[0] if isinstance(h, container.ErrorbarContainer) else h for h in handles]

    ax.legend(handles, labels, fontsize = 'x-large', loc = 'upper left')
    ax.tick_params(labelsize = 18)
    ax.set_xlabel('Strom I in mA', fontsize = 18)
    ax.set_ylabel(r'Leistung am Detektor in mW', fontsize = 18)

    ax.set_xbound(0,46)
    ax.set_ybound(-0.1,2.3)
    plt.subplots_adjust(left = 0.13, bottom = 0.14, right = 0.99, top = 0.99)

    fig, ax = plt.subplots()
    #nu = 10**9*c/670
    last = 6
    #p2 = unp.uarray(p[-last:], p_err[-last:])
    #i = unp.uarray(i[-last:], i_err[-last:])
    i_probe = np.linspace(36, 45, 100)
    p2 = linear(output_after.beta, i_probe)
    nd = 670*10**9*p2*e/(h*c*(i_probe-i_s))
    ax.plot(i_probe, unp.nominal_values(nd), 'b-')

def plot_nd():
    i, p, i_err, p_err = get_data_laser()
    fig, ax = plt.subplots()
    fmtr = ShorthandFormatter()
    
    last = 8
    output_after = fit_odr(linear, i[-last:-2], p[-last:-2], i_err[-last:-2], p_err[-last:-2], [1,1])

    a = ufloat(output_before.beta[0], output_before.sd_beta[0])
    b = ufloat(output_before.beta[1], output_before.sd_beta[1])
    c = ufloat(output_after.beta[0], output_after.sd_beta[0])
    d = ufloat(output_after.beta[1], output_after.sd_beta[1])
    i_s = (d-b)/(a-c)

    x_fit = np.linspace(30,45,2)

    ax.errorbar(i, p, xerr = i_err, yerr = p_err, fmt = 'b.', capsize = 2, label = 'Messpunkte')
    ax.plot(x_fit, linear(output_after.beta, x_fit), 'r-', label = r'linearer Fit nach $I_s$')
    ax.plot([i_s.nominal_value, i_s.nominal_value], [-1,3], 'k--', linewidth = 1, label = fmtr.format(r'$I_s={0:.1u}$mA', i_s))

    ax.fill_between([i_s.nominal_value-i_s.std_dev, i_s.nominal_value+i_s.std_dev], -1, 4, color = 'k', alpha = 0.1)
    
    ax.legend(fontsize = 'x-large')
    ax.tick_params(labelsize = 18)
    ax.set_xlabel('Strom I in mA', fontsize = 18)
    ax.set_ylabel(r'Leistung am Detektor in mW', fontsize = 18)

    ax.set_xbound(34,46)
    ax.set_ybound(-0.1,2.3)
    plt.subplots_adjust(left = 0.13, bottom = 0.14, right = 0.99, top = 0.99)

def plot_gitter():
    x, p, x_err, p_err = get_data_gitter()
    fig, ax = plt.subplots()
    fmtr = ShorthandFormatter()
    x_fit = np.linspace(min(x)-2, max(x)+2, 300)

    output = fit_odr(gaussian, x, p, x_err, p_err, [1,1,0])

    A = ufloat(output.beta[0], output.sd_beta[0])
    sigma = ufloat(output.beta[1], output.sd_beta[1])
    xo = ufloat(output.beta[2], output.sd_beta[2])

    fwhm = 2*np.sqrt(2*np.log(2))*sigma
    dlambda = calculate_delta_lambda(fwhm)
    print ('dlambda = {} nm'.format(dlambda))

    ax.errorbar(x, p, xerr = x_err, yerr = p_err, fmt = 'bs', capsize = 2, label = 'Messpunkte', markersize = 5)
    ax.plot(x_fit, gaussian(output.beta, x_fit), 'r-', label = r'Fit mit Gausskurve', linewidth = 1.2)# $y=Ae^\frac{(x-x_0)^2}{2\sigma^2}$')
    ax.plot([],[], ' ', label = fmtr.format(r'$\sigma = {:.1u}$ mm', sigma))
    ax.plot([],[], ' ', label = fmtr.format(r'FWHM = {:.1u} mm', fwhm))
    ax.plot([],[], ' ', label = fmtr.format(r'$\Delta\, \lambda = {:.1u}$ nm', dlambda))
    ax.plot([-12,12],[0,0], 'k-', linewidth = 0.5)

    ax.legend(fontsize = 13, loc='upper left')
    ax.tick_params(labelsize = 18)
    ax.set_xlabel('Abstand vom Maximum in mm', fontsize = 18)
    ax.set_ylabel(r'Leistung am Detektor in $\mu$W', fontsize = 18)

    ax.set_xbound(-12.8,10.4)
    ax.set_ybound(-0.015,0.28)
    plt.subplots_adjust(left = 0.16, bottom = 0.14, right = 0.99, top = 0.99)


def calculate_delta_lambda(deltax):
    d = 1/1200 #gitterkonstante in mm
    wavelength = 670*10**(-6) #alles in mm
    beta = np.arcsin(wavelength/(2*d)) #kommt weil die -1ste Beugungsordnung wieder im Laser gestreut wird. Einsetzen in Formel (6) vom Skript
    k = 1 #beugungsordnung
    l = ufloat(149,1) #abstand von Gitter zu Detektor
    return 2*d*deltax*np.cos(beta)/(k*l)*10**6 #return in nm statt mm

if __name__ == '__main__':
    plot_laser()
    plot_gitter()
    plt.show()
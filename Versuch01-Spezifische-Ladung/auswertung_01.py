#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
import scipy.constants as cns
import matplotlib.ticker

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu

fmtr = ShorthandFormatter()

def linear(B,x):
    return B[0]*x+B[1]

def linear_2(B,x):
    return B[0]*x

def exponential(B,x):
    return B[0]*np.exp(B[1]*x)

def stdev(n):
    mean = sum(n)/len(n)
    return np.sqrt(sum((mean-np.array(n))**2/(len(n)-1)))

def get_data(voltage):
    xls = pd.ExcelFile('./daten_01.xlsx')
    df = pd.read_excel(xls, sheet_name = voltage)
    i = unp.uarray(df['i'], df['i_err'])
    u = unp.uarray(df['u'], df['u_err'])
    #r = (unp.uarray(df['r'], df['r_err'])-unp.uarray(df['l'], df['l_err']))/2
    r = (unp.uarray(df['r'], df['r_err'])-np.array(df['l']))/2
    n = 100
    h = 8*n*i/(np.sqrt(125)*0.14)
    return i, u, r, h

def calculate_dx(s):
    n = 1.5
    d = 0.05
    R = ufloat(14.3, 0.5)
    alpha = unp.arccos(s/R)
    beta = unp.arcsin(unp.sin(alpha/n))
    l = d/unp.cos(beta)
    return l*unp.sin(alpha-beta)

def get_radius_corr(voltage):
    xls = pd.ExcelFile('./daten_01.xlsx')
    df = pd.read_excel(xls, sheet_name = voltage)
    r = unp.uarray(df['r'], df['r_err'])
    l = unp.uarray(df['l'], df['l_err'])
    center = 15

    return (r-l+calculate_dx(r-center)+calculate_dx(l-center))/2


def calculate_em(output, voltage):
    A = ufloat(output.beta[0], output.sd_beta[0])/10000
    R = 0.14 #in meter
    N = 100
    mu_0 = cns.mu_0
    U = ufloat(sum(voltage)/len(voltage), stdev(voltage))
    return 250*U*(R/(8*N*mu_0))**2/A

def plot(voltage):
    i, u, r, h = get_data(voltage)

    output = fit_odr(linear, unp.nominal_values(1/i**2), unp.nominal_values(r**2), unp.std_devs(1/i**2), unp.std_devs(r**2), [0,0])
    #output = fit_odr(linear, unp.nominal_values(i**2), unp.nominal_values(1/r**2), unp.std_devs(i**2), unp.std_devs(1/r**2), [0,0])
    output_2 = fit_odr(linear_2, unp.nominal_values(1/i**2), unp.nominal_values(r**2), unp.std_devs(1/i**2), unp.std_devs(r**2), [0,0])
    print(fmtr.format('{}V with Offset: e/m = {:.1u}', voltage, calculate_em(output, unp.nominal_values(u))))
    print(fmtr.format('{}V without Offset: e/m = {:.1u}', voltage, calculate_em(output_2, unp.nominal_values(u))))

    x_fit = np.linspace(min(unp.nominal_values(1/i**2)), max(unp.nominal_values(1/i**2)), 2)
    #x_fit = np.linspace(min(unp.nominal_values(i**2)), max(unp.nominal_values(i**2)), 2)

    fig, ax = plt.subplots()
    ax.errorbar(unp.nominal_values(1/i**2), unp.nominal_values(r**2), xerr = unp.std_devs(1/i**2), yerr = unp.std_devs(r**2), fmt = 'b.', capsize = 2, linewidth = 1, label = 'Messpunkte')
    #ax.errorbar(unp.nominal_values(i**2), unp.nominal_values(1/r**2), xerr = unp.std_devs(i**2), yerr = unp.std_devs(1/r**2), fmt = 'b.', capsize = 2, linewidth = 1, label = 'Messpunkte')
    #ax.plot(x_fit, linear(output.beta, x_fit), 'r-', label = r'$y=Ax+b$')
    #ax.plot([],[], ' ', label = fmtr.format(r'A={}', ufloat(output.beta[0], output.sd_beta[0])))
    ax.plot(x_fit, linear_2(output_2.beta, x_fit), 'r-', label = r'$y=Ax$')
    ax.plot([],[], ' ', label = fmtr.format(r'A={:.1u} cm$^2\cdot$A$^2$', ufloat(output_2.beta[0], output_2.sd_beta[0])))
    ax.tick_params(labelsize = 18)

    ax.set_xlabel(r'$1/I^2$ in A$^{-2}$', fontsize = 18)
    ax.set_ylabel('$r^2$ in cm$^2$', fontsize = 18)

    ax.legend(loc = 'upper left', fontsize = 'xx-large')

    plt.subplots_adjust(left = 0.13, bottom = 0.16, right = 0.99, top = 0.98)

def calculate_em_h(output):
    A = ufloat(output.beta[0], output.sd_beta[0])*10000000000
    return A

def plot_h(voltage):
    i, u, r, h = get_data(voltage)
    r_corr = get_radius_corr(voltage)
    U = sum(u)/len(u)
    h = h*cns.mu_0*1000
    
    output = fit_odr(linear, unp.nominal_values(h**2), unp.nominal_values(2*u/r**2), unp.std_devs(h**2), unp.std_devs(2*u/r**2), [0,0])
    output_2 = fit_odr(linear_2, unp.nominal_values(h**2), unp.nominal_values(2*u/r**2), unp.std_devs(h**2), unp.std_devs(2*u/r**2), [0,0])
    output_2_corr = fit_odr(linear_2, unp.nominal_values(h**2), unp.nominal_values(2*u/r_corr**2), unp.std_devs(h**2), unp.std_devs(2*u/r_corr**2), [0,0])

    print (fmtr.format('relative difference of uncorrected and corrected: {:.3f}', 1-output_2.beta[0]/output_2_corr.beta[0]))
    print(fmtr.format('{}V without Correction: e/m = {:.1u}', voltage, calculate_em_h(output_2)))
    print(fmtr.format('{}V with Correction: e/m = {:.1u}', voltage, calculate_em_h(output_2_corr)))

    x_fit = np.linspace(min(unp.nominal_values(h**2)), max(unp.nominal_values(h**2)), 2)

    fig, ax = plt.subplots()

    #ax.errorbar(unp.nominal_values(h**2), unp.nominal_values(2*u/r**2), xerr = unp.std_devs(h**2), yerr = unp.std_devs(2*u/r**2), fmt = '^', color = '#DD9910', capsize = 2, linewidth = 1, label = 'Unkorrigierte Messpunkte')
    #ax.errorbar(unp.nominal_values(h**2), unp.nominal_values(2*u/r_corr**2), xerr = unp.std_devs(h**2), yerr = unp.std_devs(2*u/r_corr**2), fmt = 'bo', capsize = 2, linewidth = 1, label = 'Korrigierte Messpunkte')
    #ax.plot(x_fit, linear(output.beta, x_fit), 'r-', label = r'$y=Ax+b$')
    #ax.plot(x_fit, linear(output_2_corr.beta, x_fit), 'r-', label = r'$y=Ax$')
    #ax.plot([],[], ' ', label = fmtr.format(r'$A={:.1u}\cdot 10^{{11}}$ $C/kg$', ufloat(output_2_corr.beta[0], 1.1*output_2_corr.sd_beta[0])/10))

    ax.errorbar(unp.nominal_values(h**2), unp.nominal_values(2*u/r**2), xerr = unp.std_devs(h**2), yerr = unp.std_devs(2*u/r**2), fmt = 'b^', capsize = 2, linewidth = 1, label = 'Messpunkte')
    ax.plot(x_fit, linear(output_2.beta, x_fit), 'r-', label = r'$y=Ax$')
    ax.plot([],[], ' ', label = fmtr.format(r'$A={:.1u}\cdot 10^{{11}}$ $C/kg$', ufloat(output_2.beta[0], 1.05*output_2.sd_beta[0])/10))
    ax.tick_params(labelsize = 18)

    ax.tick_params(labelsize = 18)

    ax.set_xlabel(r'$B^2$ in (mT)$^2$', fontsize = 18)
    ax.set_ylabel('$2U/r^2$ in V/cm$^2$', fontsize = 18)

    ax.legend(loc = 'upper left', fontsize = 'x-large')

    plt.tight_layout()
    #plt.subplots_adjust(left = 0.13, bottom = 0.16, right = 0.99, top = 0.98)

if __name__ == '__main__':
    plot_h('250')
    plot_h('300')
    plt.show()
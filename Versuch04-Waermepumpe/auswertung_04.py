#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare
#import scipy.interpolate as interp

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu

def linear2(x,a,b):
    return a*x+b

def quadratic(B, x):
    return B[0]*x**2+B[1]*x+B[2]

def quadratic2(x,a,b,c):
    return a*x**2+b*x+c

def cubic(B, x):
    return B[0]*x**3+B[1]*x**2+B[2]*x+B[3]

def cubic2(x,a,b,c,d):
    return a*x**3+b*x**2+c*x+d

def get_dataframe(dataset, header = 0):
    xls = pd.ExcelFile("./datasets/" + dataset)
    df = pd.read_excel(xls, header=header)
    return df

def get_data_sensor(): #es wurde bis 21 Minuten aufgenommen, die letzten 9 Punkte sind spater
    df = get_dataframe('sensor_log.xlsx', 7)
    time = df['Seconds']*1.
    chs = [df['CHANNEL'+str(i)][:-9] for i in range(8)]
    return np.array(time[:-9]), np.array(chs)

def get_data_off(): #es wurde bis 21 Minuten aufgenommen, die letzten 9 Punkte sind spater
    df = get_dataframe('log_pump_off.xlsx', 7)
    time = df['Seconds']*1. + 16*60
    chs = [df['CHANNEL'+str(i)] for i in range(8)]
    return np.array(time), np.array(chs)

def get_data_pressure(pressure = False):
    df = get_dataframe('pressure.xlsx')
    time = df['time in min']*60.
    time_err = 15.
    power = df['power in W']
    power_err = df['power_err']
    p_h = df['p_w in bar'] #converted to Pa
    p_h_err = df['p_w_err']
    p_c = df['p_k in bar']
    p_c_err = df['p_k_err']
    if pressure:
        return np.array(p_h), np.array(p_h_err), np.array(p_c), np.array(p_c_err)
    else:
        return np.array(time), time_err, np.array(power), np.array(power_err)

def calculate_compressor_energy(time, power, with_error = True):
    if with_error:
        e = unp.uarray(np.zeros(len(time)), np.zeros(len(time)))
    else:
        e = np.zeros(len(time))
    for i in range(1,len(time)):
        e[i] = e[i-1]+(time[i]-time[i-1])*power[i]
    return e/1000

def calculate_err(l, skip, s_range):
    #l_err = [abs(l[skip*i+s_range]-l[skip*i-s_range]) for i in range(skip,len(l)-skip)]
    l_err = [abs(l[skip*i+s_range]-l[skip*i-s_range]) for i in range(1,len(l[::skip])-1)]
    l_err.insert(0,l_err[0])
    l_err.append(l_err[-1])
    return np.array(l_err)

def get_heat_datapoints(ret_popt = False):
    time, chs = get_data_sensor()
    time_p, time_p_err, power, power_err = get_data_pressure()
    c_w = 4.182 #J/kg K
    m_hot = ufloat(4.58, 0.03)
    m_cold = ufloat(4.22, 0.03)

    heat_hot = (chs[1]-chs[1][0])*c_w*m_hot
    #heat_hot = chs[1]*c_w*m_hot
    heat_hot_err = unp.std_devs(heat_hot)
    heat_hot = unp.nominal_values(heat_hot)
    heat_cold = (chs[5]-chs[5][0])*c_w*m_cold
    #heat_cold = chs[5]*c_w*m_cold
    heat_cold_err = unp.std_devs(heat_cold)
    heat_cold = unp.nominal_values(heat_cold)

    compressor_energy = calculate_compressor_energy(unp.uarray(time_p, time_p_err), unp.uarray(power, power_err)) #in kJ
    compressor_energy_err = unp.std_devs(compressor_energy)
    compressor_energy = unp.nominal_values(compressor_energy)

    skip = int(len(heat_hot)/len(compressor_energy))+1 #12
    heat_hot_reduced = heat_hot[::skip]
    heat_cold_reduced = heat_cold[::skip]
    heat_hot_reduced_err = np.sqrt(calculate_err(heat_hot, skip, 2)**2+heat_hot_err[::skip]**2)
    heat_cold_reduced_err = np.sqrt(calculate_err(heat_cold, skip, 2)**2+heat_cold_err[::skip]**2)

    fit_func = quadratic2
    popt_h, cov_h = curve_fit(fit_func, compressor_energy, heat_hot_reduced, sigma = heat_hot_reduced_err)
    popt_c, cov_c = curve_fit(fit_func, compressor_energy, heat_cold_reduced, sigma = heat_cold_reduced_err)

    print (popt_h, [np.sqrt(cov_h[n][n]) for n in range(len(cov_h))])
    print (popt_c, [np.sqrt(cov_c[n][n]) for n in range(len(cov_c))])
    x_fit = np.linspace(min(compressor_energy)-5, max(compressor_energy)+50, 300)
    y_fit_h = fit_func(x_fit, *popt_h)
    y_fit_c = fit_func(x_fit, *popt_c)

    if ret_popt:
        return compressor_energy, compressor_energy_err, popt_c, popt_h, skip
    else:
        return compressor_energy, compressor_energy_err, heat_hot_reduced, heat_hot_reduced_err, heat_cold_reduced, heat_cold_reduced_err, x_fit, y_fit_h, y_fit_c

def return_clausius_rankine():
    return 1

def write_files():
    time, chs = get_data_sensor()
    p_h, p_h_err, p_c, p_c_err = get_data_pressure(pressure = True)
    compressor_energy, compressor_energy_err, popt_c, popt_h, skip = get_heat_datapoints(ret_popt = True)

    derivative_h = linear2(unp.uarray(compressor_energy,compressor_energy_err), 2*popt_h[0], popt_h[1])
    derivative_c = linear2(unp.uarray(compressor_energy,compressor_energy_err), 2*popt_c[0], popt_c[1])

    e_h, e_h_err = abs(unp.nominal_values(derivative_h)), unp.std_devs(derivative_h)
    e_c, e_c_err = abs(unp.nominal_values(derivative_c)), unp.std_devs(derivative_c)

    t_hot = unp.uarray(chs[1][::skip], calculate_err(chs[1], skip, 2))
    t_cold = unp.uarray(chs[5][::skip], calculate_err(chs[5], skip, 2))
    ti = np.zeros((8, len(t_hot)))

    #calculates leistungszahlen
    ec_h_u = 1/(1-t_cold/t_hot) #carnot leistungszahlen mit fehler
    ec_c_u = 1/(t_hot/t_cold-1)
    ec_h, ec_h_err = unp.nominal_values(ec_h_u), unp.std_devs(ec_h_u)
    ec_c, ec_c_err = unp.nominal_values(ec_c_u), unp.std_devs(ec_c_u)

    fmtr = ShorthandFormatter()
    csv = open("./leistungszahlen.csv", 'w')
    csv.write("E und E' sind die effektive Leistungszahlen von warm bzw. kalt. Ec und Ec' sind die Leistungszahlen vom Carnot Kreisprozess.\n\n")
    
    csv.write("Minuten,")
    csv.write("Tw in K,Tw_err,Tk in K,Tk_err,")
    csv.write("p_w in bar,p_w_err,p_k in bar,p_k_err,")
    csv.write("E,E_err,E',E'_err,")
    csv.write("Ec,Ec_err,Ec,Ec'_err\n")
    for n in range(len(compressor_energy)):
        csv.write("{},".format(n))
        csv.write("{:.3f},{:.3f},{:.3f},{:.3f},".format(t_hot[n].nominal_value, t_hot[n].std_dev, t_cold[n].nominal_value, t_cold[n].std_dev))
        csv.write("{:.3f},{:.3f},{:.3f},{:.3f},".format(p_h[n], p_h_err[n], p_c[n], p_c_err[n]))
        csv.write("{:.3f},{:.3f},{:.3f},{:.3f},".format(e_h[n], e_h_err[n], e_c[n], e_c_err[n]))
        csv.write("{:.3f},{:.3f},{:.3f},{:.3f}\n".format(ec_h[n], ec_h_err[n], ec_c[n], ec_c_err[n]))
    csv.close()

    tex = open("./leistungszahlen.txt", "w")
    tex.write("Zeit in Minuten & $T_W$ in K & $T_K$ in K & ")
    tex.write("$\epsilon_E$ & $\epsilon_E'$ & $\epsilon_C$ & $\epsilon_C'$ \\\\ \n")
    for n in [5,10,20]:
        tex.write(fmtr.format("{} $\pm 10 s$ & ", n))
        tex.write(fmtr.format("{0:.1u} & ", t_hot[n]))
        tex.write(fmtr.format("{0:.1u} & ", t_cold[n]))
        tex.write(fmtr.format("{0:.1u} & ", ufloat(e_h[n], e_h_err[n])))
        tex.write(fmtr.format("{0:.1u} & ", ufloat(e_c[n], e_c_err[n])))
        tex.write(fmtr.format("{0:.1u} & ", ufloat(ec_h[n], ec_h_err[n])))
        tex.write(fmtr.format("{0:.1u} \\\\ \n", ufloat(ec_c[n], ec_c_err[n])))
    tex.close()

def write_states_file():
    time, chs = get_data_sensor()
    p_h, p_h_err, p_c, p_c_err = get_data_pressure(pressure = True)
    skip = 12 #aufpassen
    fmtr = ShorthandFormatter()
    #header = ['time','after compressor', 'water warm', 'after warm', 'before warm', 'after valve', 'water cold', 'after cold 1', 'after cold 2']
    chs_reduced = [ch[::skip] for ch in chs]
    chs_reduced_err = [calculate_err(ch, skip, 2) for ch in chs]

    csv = open("./states.csv", "w")
    csv_c = open("./states_compact.csv", "w")

    csv.write('pressures are in MPa and temperatures are in K\n')
    csv.write('time,p hot,p hot err,p cold,p cold err,')
    csv.write('after compressor,err,water warm,err,after warm,err,before warm,err,')
    csv.write('after valve,err,water cold,err,after cold 1,err,after cold 2,err\n')

    for i in range(len(chs_reduced[0])):
        csv.write(str(i) + ',')
        csv.write('{},{},{},{},'.format(p_h[i]/10, p_h_err[i]/10, p_c[i]/10, p_c_err[i]/10))
        for n in range(len(chs_reduced)):
            csv.write('{:.2f},'.format(chs_reduced[n][i]))
            csv.write('{:.2f},'.format(chs_reduced_err[n][i]))
        csv.write('\n')
    csv.close()

    csv_c.write('Clausius Rankine Cycle: pressures are in MPa and temperatures are in K\n')
    csv_c.write('Cycle state:,2 and 3,4 and 1,2,nan,3,2,4,nan,1,1\n')
    csv_c.write('time,p hot,p cold,')
    csv_c.write('after compressor,water warm,after warm,before warm,')
    csv_c.write('after valve,water cold,after cold 1,after cold 2\n')
    for i in range(len(chs_reduced[0])):
        csv_c.write(str(i) + ',')
        csv_c.write(fmtr.format('{0:.1u},', ufloat(p_h[i]/10, p_h_err[i]/10)))
        csv_c.write(fmtr.format('{0:.1u},', ufloat(p_c[i]/10, p_c_err[i]/10)))
        for n in range(len(chs_reduced)):
            csv_c.write(fmtr.format('{0:.1u},',ufloat(chs_reduced[n][i],chs_reduced_err[n][i])))
        csv_c.write('\n')
    csv_c.close()

def plot_heat():
    compressor_energy, compressor_energy_err, heat_hot_reduced, heat_hot_reduced_err, heat_cold_reduced, heat_cold_reduced_err, x_fit, y_fit_h, y_fit_c = get_heat_datapoints()

    fig, axs = plt.subplots(ncols = 2)
    #axs[0].plot(compressor_energy, heat_hot_reduced, 'r*')
    axs[0].errorbar(compressor_energy, heat_hot_reduced, xerr = compressor_energy_err, yerr = heat_hot_reduced_err, fmt='r.', capsize = 2, label = u'aufgenommene Wärme')
    axs[0].plot(x_fit, x_fit, 'k-', label = r'Gerade mit $y=x$')
    axs[0].plot(x_fit, y_fit_h, 'r-', linewidth = 1, label = 'Fit mit Polynom 2. Grades')

    #axs[1].plot(compressor_energy, heat_cold_reduced, 'b*')
    axs[1].errorbar(compressor_energy, heat_cold_reduced, xerr = compressor_energy_err, yerr = heat_cold_reduced_err, fmt='b.', capsize = 2,  label = u'abgegebene Wärme')
    axs[1].plot(x_fit, -x_fit, 'k-', label = r'Gerade mit y=-x')
    axs[1].plot(x_fit, y_fit_c, 'b-', linewidth = 1, label = 'Fit mit Polynom 2. Grades')

    for ax in axs:
        ax.tick_params(labelsize = 18)
        ax.set_xbound(0,176)
    
    axs[0].legend(loc = 'upper left', fontsize = 'x-large')
    axs[1].legend(loc = 'upper right', fontsize = 'x-large')
    axs[0].set_xlabel('Vom Kompressor verbrauchte Energie in kJ', fontsize = 18)
    axs[0].set_ylabel(u'Wärme in kJ', fontsize = 18)
    axs[0].set_ybound(0,410)
    axs[1].set_ybound(-400,10)
    
    fig.set_size_inches((12,6))
    plt.subplots_adjust(left = 0.08, right = 0.99, top = 0.99, bottom = 0.12)

def plot_heat_one():
    compressor_energy, compressor_energy_err, heat_hot_reduced, heat_hot_reduced_err, heat_cold_reduced, heat_cold_reduced_err, x_fit, y_fit_h, y_fit_c = get_heat_datapoints()

    fig, ax = plt.subplots()
    ax.errorbar(compressor_energy, -heat_cold_reduced, xerr = compressor_energy_err, yerr = heat_cold_reduced_err, fmt='b.', capsize = 2,  label = u'abgegebene Wärme (negativ)')
    ax.errorbar(compressor_energy, heat_hot_reduced, xerr = compressor_energy_err, yerr = heat_hot_reduced_err, fmt='r.', capsize = 2, label = u'aufgenommene Wärme')
    ax.plot(x_fit,x_fit, 'k-', label = r'Gerade mit $y=x$')
    ax.plot(x_fit, y_fit_h, 'r-', linewidth = 1)
    ax.plot(x_fit, -y_fit_c, 'b-', linewidth = 1)

    ax.tick_params(labelsize = 18)

    ax.legend(loc = 'upper left', fontsize = 'x-large')
    ax.set_xlabel('Vom Kompressor verbrauchte Energie in kJ', fontsize = 18)
    ax.set_ylabel(u'Wärme in kJ', fontsize = 18)
    ax.set_xbound(0,176)
    ax.set_ybound(0,410)

    #fig.set_size_inches((12,6))
    plt.subplots_adjust(left = 0.14, right = 0.99, top = 0.99, bottom = 0.14)

def plot_off():
    time, chs = get_data_off()

    t = chs[1]
    popt, cov = curve_fit(linear2, time,t)
    print (popt[0], np.sqrt(cov[0][0]))
    #fig, axs = plt.subplots(ncols = len(chs))
    #for n in range(len(axs)):
    #    axs[n].plot (time, chs[n], 'b.')

if __name__=='__main__':
    #write_states_file()
    #write_files()
    plot_heat()
    #plot_heat_one()
    #plot_off()
    plt.show()
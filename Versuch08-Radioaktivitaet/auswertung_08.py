#-*- coding: utf8 -*-
import numpy as np
import scipy.odr.odrpack as odrpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib import container
from os import listdir
import pandas as pd
from uncertainties import ufloat
import uncertainties.unumpy as unp
from uncertainties.umath import *
import uncertainties as uc
from scipy.stats import chisquare
import matplotlib.ticker

import sys
sys.path.insert(0,"../")
from utils import ShorthandFormatter, fit_odr, relu

folder = './daten/'
fmtr = ShorthandFormatter()

def linear(B,x):
    return B[0]*x+B[1]

def exponential(B,x):
    return B[0]*np.exp(B[1]*x)

def get_t_r(f, interval):
    l = (f.read()).split('\n')
    while '' in l: l.remove('')
    t = [float(l[i].split('\t')[0])/1000 for i in range(4,len(l))]
    r = [float(l[i].split('\t')[1]) for i in range(4,len(l))]

    if t[-1]-t[-2]<interval-1: t.pop(-1), r.pop(-1)
    t.append(t[-1]+interval)
    return np.array(t), np.array(r)

def read_rate(f, interval):
    t,r = get_t_r(f,interval)
    n_tot = sum([r[i]*(t[i+1]-t[i])/60 for i in range(len(r))])
    if n_tot ==0:
        n_tot_err = 0
    else:
        n_tot_err = np.sqrt(n_tot)
    t_tot = t[-1]
    return 60*n_tot/t_tot, 60*n_tot_err/t_tot #returns rate and rate error in 1/min using poisson statistics

def get_data_characteristic():
    voltages = list(range(100,701,100)) + [720,750,775] + list(range(800,1201,100))
    fls = [open(folder + 'Charakteristik_{}V.txt'.format(i),'r') for i in voltages]
    voltages_err = 5

    r_all = list(zip(*[read_rate(f,15) for f in fls]))
    for f in fls: f.close()
    r, r_err = np.array(r_all[0]), np.array(r_all[1])

    return voltages, voltages_err, r, r_err

def plot_characteristic():
    v, v_err, r, r_err = get_data_characteristic()
    fig, ax = plt.subplots()
    print (r[-1], r_err[-1])
    ax.errorbar(v,r, xerr = v_err, yerr = r_err, fmt='bs', capsize = 3, label = 'Messpunkte', markersize = 4)

    ax.tick_params(labelsize = 18)
    ax.set_xticks(range(0,1201,100))

    ax.set_xbound(600,1250)
    ax.set_ybound(0,1450)

    ax.set_xlabel('Spannung in V', fontsize = 18)
    ax.set_ylabel(u'Zählrate in Ereignisse/min', fontsize = 18)
    ax.legend(loc = 'upper left', fontsize = 14)
    plt.subplots_adjust(left = 0.17, bottom = 0.14, right = 0.98, top = 0.99)

def stdev(n):
    mean = sum(n)/len(n)
    return np.sqrt(sum((mean-np.array(n))**2/(len(n)-1)))

def get_data_poission():
    f = open(folder + 'poisson.txt', 'r')
    t,r = get_t_r(f,15)

    t2 = [(t[i]+t[i-1])/2 for i in range(1,len(t))]
    n = r/4

    n_avg = sum(n)/len(n)
    sigma = stdev(n)
    sigma_err = sigma/np.sqrt(2*(len(n)-1))

    txt = open('./poisson_results.txt', 'w')
    txt.write('Time bin = 15s, N = {}\n\n'.format(len(n)))
    txt.write('average n = {:.2f}\n'.format(n_avg))
    txt.write('sigma_n = {:.2f}, sigma_n uncertainty = {:.3f}\n'.format(sigma, sigma_err))
    txt.write('sigma_P = {:.2f}\n\n'.format(np.sqrt(n_avg)))
    txt.write('total events = {:.0f}\n'.format(sum(n)))
    txt.write('sigma_N = {:.1f}\n\n'.format(np.sqrt(sum(n))))
    txt.write(fmtr.format('Vergleich:\tsigma_N/N = {:.3f}, sigma_P = {:.2f}, sigma_n = {:.1u}',np.sqrt(sum(n))/len(n), np.sqrt(n_avg), ufloat(sigma, sigma_err)))
    txt.close()

    return np.array(t2), np.array(n)

def plot_poisson():
    t, n = get_data_poission()
    n_avg = sum(n)/len(n)
    sigma = stdev(n)

    interval_length = 2
    n_axis = np.arange(min(n)-1, max(n)+3)
    x_axis = np.arange(min(n)-1, max(n)+3, interval_length)

    h1 = np.zeros(len(n_axis)) #haufigkeit
    for i in range(len(n_axis)):
        for j in n:
            if n_axis[i] == j: h1[i]+=1
    h1/=(sum(h1))

    p1 = np.array([np.exp(-n_avg)*(n_avg**x)/factorial(x) for x in n_axis])
    g1 = np.exp(-((n_axis-n_avg)**2/(2*sigma**2)))/(np.sqrt(2*np.pi)*sigma)

    p = [sum([p1[i*interval_length+a] for a in range(interval_length)])/interval_length for i in range(len(x_axis))]
    g = [sum([g1[i*interval_length+a] for a in range(interval_length)])/interval_length for i in range(len(x_axis))]
    h = [sum([h1[i*interval_length+a] for a in range(interval_length)])/interval_length for i in range(len(x_axis))]

    print(sum(h1))
    print(sum(h))
    fig, ax = plt.subplots()
    #step = 0.2
    #multi = 1.8
    #offset = 0
    #for i in range(1,len(x_axis)):
    #    ax.fill_between([x_axis[i]-step*multi-offset, x_axis[i]+step*multi-offset], h[i], step = 'mid', color = 'b', alpha = 0.9)
    #    ax.fill_between([x_axis[i]-step, x_axis[i]+step], p[i], step = 'mid', color = 'g', alpha = 0.5)
    #    ax.fill_between([x_axis[i]-step+offset, x_axis[i]+step+offset], g[i], step = 'mid', color = '#FF9910', alpha = 0.5)
#
    #ax.fill_between([x_axis[0]-step-offset, x_axis[0]+step-offset], h[0], step = 'mid', color = 'b', alpha = 0.9, label = 'Daten')
    #ax.fill_between([x_axis[0]-step, x_axis[0]+step], p[0], step = 'mid', color = 'g', alpha = 0.5, label = 'Poissonverteilung')
    #ax.fill_between([x_axis[0]-step+offset, x_axis[0]+step+offset], g[0], step = 'mid', color = '#FF9910', alpha = 0.5, label = 'Gaussverteilung')

    ax.hist(n, x_axis+1, align = 'mid', density = True, rwidth=0.8, color = '#FF9910', alpha = 0.8, label = 'Messdaten')
    #ax.plot(x_axis, h, 's', color = 'k', alpha = 0.9, label = 'Messdaten')
    #ax.plot(x_axis, p, 'o', color = 'r', alpha = 0.9, label = 'Poissonverteilung')
    #ax.plot(x_axis, g, '^', color = 'b', alpha = 0.9, label = 'Gaussverteilung')
    ax.plot(n_axis, p1, 'o', color = 'r', alpha = 0.9, label = 'Poissonverteilung')
    ax.plot(n_axis, g1, '^', color = 'b', alpha = 0.9, label = 'Gaussverteilung')

    ax.tick_params(axis = 'x', labelsize = 18)
    ax.tick_params(axis = 'y', labelsize = 14)
    #ax.set_xticks(x_axis[1::2])
    ax.set_xticks(x_axis)
    ax.set_xlabel(u'Ereignisse n pro Zeitbin', fontsize = 18)
    ax.set_ylabel(u'Relative Häufigkeit', fontsize = 18)
    ax.legend(loc = 'upper right', fontsize = 'large')
    plt.subplots_adjust(left = 0.14, bottom = 0.14, right = 0.99, top = 0.99)

def get_data_background():
    f = open(folder + 'hintergrund.txt', 'r')
    th, rh = get_t_r(f, 60)
    f.close()

    nh_tot = sum(rh)
    nh_err = np.sqrt(nh_tot)
    rh, rh_err = 60*nh_tot/th[-1], 60*nh_err/th[-1]
    return rh, rh_err

def get_data_barium():
    f = open(folder + 'barium.txt', 'r')
    t,r = get_t_r(f,15)
    f.close()
    t2 = np.array([(t[i]+t[i-1])/2 for i in range(1,len(t))])

    r_err = 4*np.sqrt(r/4) #weil n=r/4

    rh, rh_err = get_data_background()
    rc = r-rh
    rc_err = np.sqrt(r_err**2 + rh_err**2)

    return t2[:-2], r[:-2], r_err[:-2], rc[:-2], rc_err[:-2]

def plot_barium():
    t, r, r_err, rc, rc_err = get_data_barium()
    t/=60 #t in minuten

    logrc = unp.log(unp.uarray(rc,rc_err))

    output = fit_odr(linear, t, unp.nominal_values(logrc), None, unp.std_devs(logrc), [0,0])
    x_fit = np.linspace(min(t), max(t)+2, 100)

    fig, ax = plt.subplots()
    ax.set_yscale('log')
    #ax.errorbar(t, r, yerr = r_err, fmt='b.', capsize = 2, label = 'Messpunkte')
    ax.errorbar(t, rc, yerr = rc_err, fmt='b.', capsize = 2, label = 'Messpunkte', linewidth = 0.75, markersize = 5)
    ax.plot(x_fit, np.exp(linear(output.beta, x_fit)), 'r-', label = r'Fit: $A(t) = A_0e^{-\lambda t}}$')
    ax.plot([], [], ' ', label = fmtr.format(r'$\lambda = {:.1u}$ min$^-$$^1$', -ufloat(output.beta[0], output.sd_beta[0])))
    ax.plot([], [], ' ', label = fmtr.format(r'$A_0 = {:.1u}$ min$^-$$^1$', exp(ufloat(output.beta[1], output.sd_beta[1]))))

    ax.set_xbound(0,19)
    ax.set_ybound(2,3000)

    ax.tick_params(labelsize = 18)
    ax.set_xlabel(u'Zeit t in Minuten', fontsize = 18)
    ax.set_ylabel(u'Korrigierte Zählrate in 1/min', fontsize = 18)
    ax.legend(loc = 'lower left', fontsize = 15)
    plt.subplots_adjust(left = 0.14, bottom = 0.14, right = 0.99, top = 0.99)

def get_data_absorption(): #uses standard deviation for error
    letters = {'':[0], '_a':range(1,6),'_b':range(1,6)}
    t,r,r_err = [[]],[[]],[[]]
    i=0
    for key in letters:
        fls = [open(folder + 'absorption{}_{}.txt'.format(key, i)) for i in letters[key]]
        for f in fls:
            l = (f.read()).split('\n')
            while '' in l: l.remove('')
            tl = [float(l[i].split('\t')[0])/1000 for i in range(4,len(l))]
            rl = [float(l[i].split('\t')[1]) for i in range(4,len(l))]

            r[i].append(sum(rl)/len(rl))
            t[i].append(tl[-1])
            r_err[i].append(stdev(rl))
            f.close()
        t.append([])
        r.append([])
        r_err.append([])
        i+=1
    return t,r,r_err

def get_data_absorption_2(): #uses poisson Statistics
    letters = {'':[0], '_a':range(1,6),'_b':range(1,6)}
    t,r,r_err = [[]],[[]],[[]]
    i=0
    for key in letters:
        fls = [open(folder + 'absorption{}_{}.txt'.format(key, i)) for i in letters[key]]
        for f in fls:
            l = (f.read()).split('\n')
            while '' in l: l.remove('')
            tl = [float(l[i].split('\t')[0])/1000 for i in range(4,len(l))]
            rl = [float(l[i].split('\t')[1]) for i in range(4,len(l))]

            t_tot = tl[-1]+60*100/rl[-1]
            t[i].append(t_tot)
            r[i].append(60*len(rl)*100/t_tot)
            r_err[i].append(60*10*np.sqrt(len(rl))/t_tot)
            f.close()
        t.append([])
        r.append([])
        r_err.append([])
        i+=1
    return t,r,r_err #[0] is with no plates, [1] is aluminium and [2] is lead

def write_absorption(material):
    d_alu = 1.6
    d_lead = 1.75
    info = {'aluminium':[1,d_alu], 'lead':[2,d_lead]}

    t,r,r_err = get_data_absorption_2()
    rh, rh_err = get_data_background()
    n = np.arange(0,6)
    d = n*info[material][1]

    t, r, r_err = np.array(t[0]+t[info[material][0]]), np.array(r[0]+r[info[material][0]]), np.array(r_err[0]+r_err[info[material][0]]) #sum of lists in np.array(***), not arrays
    tc, rc, rc_err = t[info[material][0]], r-rh, np.sqrt(r_err**2+rh_err**2) #sum of lists in np.array(***), not arrays

    txt = open('./absorption_{}_tabelle.txt'.format(material), 'w')
    txt.write(u'Zählrate & Korrigierte Zählrate & log & d in mm\\\\\n')
    for i in range(len(rc)):
        txt.write(fmtr.format('{:.1u} & {:.1u} & {:.1u} & {:.1u}\\\\\n', ufloat(r[i],r_err[i]), ufloat(rc[i], rc_err[i]), log(ufloat(rc[i], rc_err[i])), ufloat(d[i], np.sqrt(n[i])*0.05 )))
    txt.write(fmtr.format('\nHintergrundstrahlung: {:.1u}\n', ufloat(rh, rh_err)))
    txt.close()

def plot_absorption(material):
    d_alu = 1.6
    d_lead = 1.75
    legend_a = 'lower left'
    legend_b = 'upper right'
    xbound_a = (-0.3,8.5)
    xbound_b = (-0.3,9)
    ybound_a = (61,111)
    ybound_b = (48,111)
    info = {'aluminium':[1,d_alu,'a', legend_a, xbound_a, ybound_a], 'lead':[2,d_lead,'b', legend_b, xbound_b, ybound_b]}

    t,r,r_err = get_data_absorption_2()
    rh, rh_err = get_data_background()

    #t, r, r_err = np.array(t[0]+t[info[material][0]]), np.array(r[0]+r[info[material][0]])-rh, np.sqrt(np.array(r_err[0]+r_err[info[material][0]])**2+rh_err**2) #sum of lists in np.array(***), not arrays
    t, r, r_err = np.array(t[0]+t[info[material][0]]), np.array(r[0]+r[info[material][0]]), np.array(r_err[0]+r_err[info[material][0]]) #sum of lists in np.array(***), not arrays
    tc, rc, rc_err = t[info[material][0]], r-rh, np.sqrt(r_err**2+rh_err**2) #sum of lists in np.array(***), not arrays
    
    n = np.arange(0,6)
    d = n*info[material][1]
    d_err = np.sqrt(n)*0.05

    x_fit = np.linspace(0,5,100)*info[material][1]

    logrc = unp.log(unp.uarray(rc, rc_err))
    output = fit_odr(linear, d, unp.nominal_values(logrc), None, unp.std_devs(logrc), [-1,0])

    fig, ax = plt.subplots()

    #ax.set_yscale('log')
    #ax.errorbar(d, rc, xerr = d_err, yerr = rc_err, fmt = 'bo', capsize = 4, label = 'Messpunkte', linewidth = 1)
    #ax.plot(x_fit, np.exp(linear(output.beta,x_fit)), 'r-', label = r'Fit: $I(x)=I_0\cdot e^{{-\mu_{} x}}$'.format(info[material][2]))

    ax.errorbar(d, unp.nominal_values(logrc), xerr = d_err, yerr = unp.std_devs(logrc), fmt = 'bo', capsize = 4, label = 'Messpunkte', linewidth = 1)
    ax.plot(x_fit, linear(output.beta,x_fit), 'r-', label = r'Fit: $I(x)=I_0\cdot e^{{-\mu_{} x}}$'.format(info[material][2]))

    ax.plot([], [], ' ', label = fmtr.format(r'$\mu_{} = {:.1u}$ mm$^{{-1}}$', info[material][2],-ufloat(output.beta[0], output.sd_beta[0])))
    ax.plot([], [], ' ', label = fmtr.format(r'$I_0 = {:.1u}$ min$^{{-1}}$', exp(ufloat(output.beta[1], output.sd_beta[1]))))


    ax.tick_params(labelsize = 18)
    #ax.set_yticks(np.arange(50,150,10))
    #ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    #ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
    
    #ax.set_xbound(info[material][4])
    #ax.set_ybound(info[material][5])

    ax.set_xlabel(u'Dicke des Materials in mm', fontsize = 18)
    ax.set_ylabel(r'$\ln(R\cdot min)$', fontsize = 18)
    #ax.set_ylabel(u'Korrigierte Zählrate in 1/min', fontsize = 18)
    ax.legend(loc = info[material][3], fontsize = 15)
    plt.subplots_adjust(left = 0.15, bottom = 0.14, right = 0.99, top = 0.99)

if __name__ == '__main__':
    plot_characteristic()
    plot_poisson()
    plot_barium()
    plot_absorption('lead')
    plot_absorption('aluminium')
    write_absorption('lead')
    write_absorption('aluminium')
    plt.show()
import pandas as pd
from uncertainties import ufloat

import sys
sys.path.insert(0,"./")
from utils import ShorthandFormatter

versuch = './Versuch07-Mikrowellen/'

xls = pd.ExcelFile(versuch + "daten_07.xlsx")
df = pd.read_excel(xls, sheet_name = 'beugung')
fmtr = ShorthandFormatter()

titles = ['alpha','beta', 's']
titles_err = ['alpha_err','beta_err', 's_err']

cols = [df[title] for title in titles]
cols_err = [df[title] for title in titles_err]

txt = open(versuch + "./texts/beugung.txt", "w")
for i in range(len(cols[0])):
    for j in range(len(cols)-1):
        txt.write(fmtr.format('{:.1u} & ', ufloat(cols[j][i], cols_err[j][i])).replace('.', ','))
    txt.write(fmtr.format('{:.1u}', ufloat(cols[-1][i], cols_err[-1][i])).replace('.', ','))
    txt.write("\\\\\n")
txt.close()
